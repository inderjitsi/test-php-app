<script>
    $(function () {
        //Vendor listing page
        $(document).on('submit', '#recordListing', function () {

            var data = $(this).serialize();

            $.post('/admin/users/setVendors', data, function (resp) {

                if (resp.html) {
                    $('#vendorTableResponsive').html(resp.html);
                }
                if (resp.actions && resp.actions == 'delete') {
                    var message = "<?= __('Vendor\'s deleted successfully.'); ?>";
                    success_alert(message);
                }

            }, 'json');

            return false;
        });
    });
</script>

<section class="content-header">
    <h1><?= __('Drivers') ?> <small><?= __('List All Drivers') ?></small></h1>
    <ol class="breadcrumb">
        <li>
            <?php echo $this->Html->link('<i class="fa fa-dashboard"></i>'.__('Home'), BASE_URL_ADMIN, array('escape' => false)); ?> 
        </li>
        <li class="active"><?= __('Drivers') ?></li>
    </ol>
</section>      

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">				

                <?php echo $this->Form->create('User', array('id' => 'recordListing')); ?>

                <?php echo $this->Form->input('page', array('type' => 'hidden', 'class' => 'page_num', 'value' => 1)); ?>
                <?php echo $this->Form->input('order_by', array('type' => 'hidden', 'class' => 'order_by', 'value' => $order_by)); ?>
                <?php echo $this->Form->input('order', array('type' => 'hidden', 'class' => 'order', 'value' => $order)); ?>

                <div class="box-body">
                    
                    <div class="box-header">
                        <?php echo $this->Html->link('<strong>'.__('Add Driver').'</strong>', ['controller' => 'Users', 'action' => 'add_vendor'], ['escape' => false, 'class' => 'pull-right']); ?>
                    </div>
                    
                    <h3><?= __('Find Driver'); ?></h3>                    
                    
                    <div class="row">
                       <div class="col-md-12">
                           <div class="col-md-4">			
                               <div class="form-group">							
                                   <label><?= __('Keyword'); ?></label>
                                   <?php                            
                                       echo $this->Form->input('keyword', array('class' => 'form-control ', 'placeholder' => __("Keyword"), 'label' => false, 'maxlength' => 60));
                                   ?>							
                               </div>
                           </div>
                           <div class="col-md-4">			
                               <div class="form-group">
                                   <label><?= __('Status'); ?></label>
                                   <?php echo $this->Form->input('status', array('class' => 'form-control select2 search-select', 'options' => array(1 => __('Active'), 2 => __('Deactive') ), 'empty' => __('Select'), 'label' => false, 'data-minimum-results-for-search' => 'Infinity')); ?>
                               </div>
                           </div>
                       </div>                        
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <?php echo $this->Form->button(__('Search'), ['class' => 'btn btn-primary', ]) ?>
                                &nbsp; &nbsp; 
                                <?php echo $this->Form->button(__('Reset'), ['class' => 'reset-btn btn btn-primary', 'type' => 'button']) ?>
                            </div>
                        </div>
                    </div>
                
                </div>
                
                <div class="box-body" id ="vendorTableResponsive">
                    <?php echo $this->Element('Admin/vendor_filter'); ?>
                </div>
                <!-- /.box-body -->

                <?php if (!empty($vendors)) { ?>	

                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-3">
                                <span class="pull-left "><?= __('Actions'); ?>:&nbsp; 
                                    <?php
                                    $options = ['2' => __('Activate'), '3' => __('Deactive'), '1' => __('Delete')];
                                    echo $this->Form->select('actions', $options, ['class' => 'actionAll select2 form-control input-sm pull-left', 'style' => 'width:150px', 'empty' => __("Select action"), 'data-minimum-results-for-search' => 'Infinity']);
                                    ?>
                                </span>
                            </div>
                            <div class="col-sm-9">
                                <div class="fa col-sm-2"> <?= __('LEGENDS'); ?>: </div>
                                <div class="col-sm-10"> 
                                    <div class="fa fa-pencil-square-o">&nbsp;<?= __('Edit'); ?> &nbsp; &nbsp; </div>
                                    <div class="fa fa-battery-half">&nbsp;<?= __('Tank Quantity'); ?>&nbsp; &nbsp;</div>
                                    <div class="fa fa-star">&nbsp;<?= __('View Rating'); ?>&nbsp; &nbsp; </div>
                                    <div class="fa fa-tasks">&nbsp;<?= __('Order History'); ?>&nbsp; &nbsp;</div>
                                    <div class="fa fa-trash">&nbsp;<?= __('Delete'); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } ?>					

                <?php echo $this->Form->end(); ?>

            </div>
        </div>
    </div>
</section>