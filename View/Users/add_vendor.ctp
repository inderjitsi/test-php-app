<?php echo $this->Html->script('admin/vendor'); ?>

<?php
$text = __('Add Driver');
if (isset($users->id) && !empty($users->id)) {
    $text = __('Edit Driver');
}
?>
<section class="content-header">
    <h1><?= $text; ?></h1>
    <ol class="breadcrumb">
        <li>
            <?php
            echo $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Home'), BASE_URL_ADMIN, array('escape' => false));
            ?> 
        </li>
        <li>
            <?php
            echo $this->Html->link('Driver', ['controller' => 'Users', 'action' => ''], ['escape' => false]);
            ?>
        </li>
        <li class="active"><?= $text; ?></li>
    </ol>
</section>  
<!-- Main content -->

<section class="content">    		

    <div class="box box-primary">
        <div class="box-header with-border">              
            <p style="margin-left:6px; font-weight:bold"><?= __('Note: All fields marked with {0} are required.', '(<i class="text-danger">*</i>)'); ?></p>
        </div><!-- /.box-header -->

        <!-- form start -->
        <?= $this->Form->create($users, array('name' => 'addEditVendor', 'id' => 'addEditVendor', 'enctype' => 'multipart/form-data')); ?>
        <?php echo $this->Form->input('id', array('type' => 'hidden', 'id' => 'user_id')); ?>

        <div class="box-body">

            <div class="row">	
                <div class="col-md-12">
                    <div class="col-md-6">			
                        <div class="form-group">							
                            <label><?= __('First Name'); ?><i class="text-danger">*</i></label>
                            <?php
                            echo $this->Form->input('first_name', array('class' => 'form-control ', 'placeholder' => __("First Name"), 'label' => false, 'maxlength' => 40));
                            ?>							
                        </div>
                    </div>                    
                </div>

                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">							
                            <label><?= __('Last Name'); ?><i class="text-danger">*</i></label>
                            <?php echo $this->Form->input('last_name', array('class' => 'form-control', 'placeholder' => __("Last Name"), 'label' => false, 'maxlength' => 40)); ?>							
                        </div>
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><?= __('Address'); ?><i class="text-danger">*</i></label>
                            <?php echo $this->Form->input('address', array('class' => 'form-control', 'placeholder' => __("Address"), 'label' => false, 'maxlength' => 80)); ?>
                        </div>
                    </div>                    
                </div>				

                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><?= __('Phone'); ?><i class="text-danger">*</i></label>
                            <?php echo $this->Form->input('phone', array('class' => 'form-control', 'placeholder' => __("Phone"), 'label' => false, 'maxlength' => 15, "data-inputmask" => '"mask": "(999) 999-9999"', "data-mask")); ?>
                        </div>
                    </div>                   
                </div>

                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><?= __('Email'); ?><i class="text-danger">*</i></label>
                            <?php echo $this->Form->input('email', array('class' => 'form-control', 'placeholder' => __("Email"), 'label' => false, 'maxlength' => 80)); ?>							
                        </div>
                    </div>                    
                </div>

                <?php /* <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><?= __('Password'); ?></label>
                            <?php echo $this->Form->input('password', array('type' => 'password', 'class' => 'form-control', 'placeholder' => __("Password"), 'label' => false, 'required' => false)); ?>							
                        </div>						
                    </div>                    
                </div>

                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><?= __('Confirm Password'); ?></label>
                            <?php echo $this->Form->input('confirm_password', array('type' => 'password', 'class' => 'form-control', 'placeholder' => __("Confirm Password"), 'label' => false)); ?>
                        </div>
                    </div>                    
                </div> */ ?>

                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><?= __('Status'); ?><i class="text-danger">*</i></label>
                            <?php echo $this->Form->input('status', array('class' => 'form-control select2', 'options' => array('1' => 'Active', '0' => 'Inactive'), 'label' => false, 'data-minimum-results-for-search' => 'Infinity')); ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><?= __('Picture'); ?></label>
                            <?php echo $this->Form->file('picture', array('class' => 'vendor-image', 'tabindex' => '18', 'label' => false)); ?>
                        </div>
                    
                        <?php
                        if (!empty($users->image)) {
                            echo $this->Form->hidden('old_image', array('value' => $users->image));
                            echo $this->Html->image(USER_PATH . $users->image);
                        }
                        ?>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <?php echo $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
                &nbsp; &nbsp; 
                <?php echo $this->Html->link(__('Cancel'), ['controller' => 'Users', 'action' => 'vendors'], ['class' => 'btn btn-primary']) ?>
            </div>

            <?= $this->Form->end(); ?>

        </div>

    </div>

</section>

<script type="text/javascript">

    jQuery.validator.addMethod("validateNullOrWhiteSpace", function (value) {
        return !isNullOrWhitespace(value);
    }, "Blanks are not allowed!");
    
    var user_id = $.trim($('#user_id').val());

    $("#addEditVendor").validate({
        ignore: [],
        rules: {
            first_name: {
                required: true,
                validateNullOrWhiteSpace: true,
            },
            last_name: {
                required: true,
                validateNullOrWhiteSpace: true,
            },
            address: {
                required: true,
                validateNullOrWhiteSpace: true,
            },
            phone: {
                required: true
            },            
            email: {
                required: true,
                remote: '/admin/users/check_driver_email/' + user_id,
                validateNullOrWhiteSpace: true,
            },
            picture: {
                accept: 'jpg,jpeg,png,gif'
            },
            password: {
                required: {
                    depends: function () {
                        return $("#password").val() !== '';
                    }
                },
                minlength: 6
            },
            confirm_password: {
                required: {
                    depends: function () {
                        return $("#password").val() !== '';
                    }
                },
                minlength: 6,
                equalTo: "#password"
            }
        },
        messages: {
            first_name: {
                required: "<?= __(FIELD_REQUIRED); ?>",
            },
            last_name: {
                required: "<?= __(FIELD_REQUIRED); ?>",
            },
            address: {
                required: "<?= __(FIELD_REQUIRED); ?>",
            },
            phone: {
                required: "<?= __(FIELD_REQUIRED); ?>",
            },
            email: {
                required: "<?= __(FIELD_REQUIRED); ?>",
                remote: "<?= __('Email id already exists.'); ?>",
            },
            picture: {
                accept: "Please select valid image jpg, jpeg, png, gif."
            }
        },
        errorElement: "div",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("div"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element) {
            $(element).parent("div").addClass("has-error");
            //$( element ).parent( "div" ).addClass( "has-error" ).removeClass( "has-success" );
        },
        unhighlight: function (element) {
            //$( element ).parent( "div" ).addClass( "has-success" ).removeClass( "has-error" );
            $(element).parent("div").removeClass("has-error");
        }
    });

</script>