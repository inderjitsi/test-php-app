<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         3.3.4
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\I18n\I18n;
use Cake\Log\Log;
use Cake\Utility\Hash;
//use Cake\Datasource\ConnectionManager;

class WebservicesController extends AppController {

    /**
     * Initialization hook method.
     *
     * @return void
     */
    public function initialize() {
        $this->loadComponent('Upload');
        $this->loadComponent('SendEmail');
    }

    function beforeFilter(Event $event) {

        //$this->autoRender = false;

        $params = $this->request->params;

        $action = $params['action'];
        $arr = array('login', 'register', 'forgot_password','check_pending_orders','test_notification','test_email');

        $headers = getallheaders();
        //prx($headers);        
        
        //set locale
        $this->setLocale($headers);

        if (!in_array($action, $arr)) { // if action not in array (authenticate user with token)			
            $token = @$headers['access_token'];

            $this->loadModel('Users');
            $user = $this->Users->find()
                    ->where(['Users.access_token' => $token, 'Users.status' => 1, 'Users.is_deleted' => 0])
                    ->select(['id', 'access_token'])
                    ->count();

            if (empty($user)) {
                $message = __("Your session has expired. Please login again.");
            }
        }

        if (!empty($message)) {

            $response['status'] = 0;
            $response['auth'] = 1;
            $response['message'] = $message;

            $this->sendData($response);
        }
    }

    /*     * ****************************************
     * Created on 28 July, 2016
     * Developer: Inderjit Singh
     * **************************************** */

    public function getData() {

        $postBody = file_get_contents('php://input');
        $data = json_decode($postBody, true);
        $data = sanitize_data($data);

        $params = $this->request->params;

        if (in_array($params['action'], array('register', 'updateProfile'))) {
            $postBody = json_encode($_POST);
        }

        if (CustomLog == 1) {

            Log::write('error', "Controller: " . $params['controller'] . ", " . "Action: " . $params['action']);
            Log::write('error', "Headers: " . json_encode(getallheaders()));
            Log::write('error', "Input Parameters: " . $postBody);

            if (!empty($_FILES)) {
                Log::write('error', "File Parameters: " . print_r($_FILES, true));
            }
        }

        return $data;
    }

    /*     * ****************************************
     * Created on 28 July, 2016
     * Developer: Inderjit Singh
     * **************************************** */

    public function sendData($data = null) {
        
        #Log::write('error', "Send Data: " . print_r($data, true));
        
        $data = $this->removeNULL($data);
        echo json_encode($data, JSON_PRESERVE_ZERO_FRACTION); die;
    }

    /* 	
     * function name 	: setLocale
     * Author        	: Inderjit Singh
     * Date          	: Sep 14, 2017     
     */

    public function setLocale($data=null) {

        $locale = 'en';
        if (!empty($data['locale']) && $data['locale'] == 'es') {
            $locale = $data['locale'];
        }
        I18n::locale($locale);
    }    

    /*     * ****************************************
     * Created on Sep 13, 2017
     * Developer: Inderjit Singh
     * **************************************** */

    public function getErrors($errors = null) {

        $error_msg = '';

        if (!empty($errors)) {
            foreach ($errors as $key1 => $error) {
                foreach ($error as $key2 => $text) {
                    $error_msg = $text;
                    break;
                }
            }
        }
        return $error_msg;
    }

    /* 	
     * function name 	: checkUser
     * Author        	: Inderjit Singh
     * Date          	: Sep 21, 2017
     * Params           : data['id'] data['user_type'] data['email'] data['password'] data['hydrate']
     */

    function checkUser($data = null) {

        $user = '';

        if (!empty($data)) {

            $where = ['Users.status' => 1, 'Users.is_deleted' => 0];

            if (!empty($data['id'])) {
                $where = array_merge($where, ['Users.id' => $data['id']]);
            }
            if (!empty($data['user_type'])) {
                $where = array_merge($where, ['Users.user_type_id' => $data['user_type']]);
            }
            if (!empty($data['email'])) {
                $where = array_merge($where, ['Users.email' => $data['email']]);
            }
            if (!empty($data['password'])) {
                $where = array_merge($where, ['Users.password' => md5($data['password'])]);
            }

            $hydrate = false;
            if (!empty($data['hydrate'])) {
                $hydrate = true;
            }

            $this->loadModel('Users');
            $user = $this->Users->find()
                    ->hydrate($hydrate)
                    ->where($where)
                    ->select(['id', 'email', 'user_type_id', 'stripe_customer_id', 'first_name', 'last_name', 'phone', 'address', 'access_token', 'image', 'latitude', 'longitude', 'online_status', 'device', 'device_token','online_date_time','online_hours'])
                    ->first();
        }
        return $user;
    }

    /* 	
     * function name 	: login
     * Author        	: Inderjit Singh
     * Date          	: Sep 14, 2017
     * input 		: {"user_type": "1/2", "email": "inderjitsi@smartdatainc.net", "password": "", "device": "android/ios", "device_token": ""}
     */

    function login() {

        $status = 0;
        $message = '';
        $info = $cards = json_decode("{}");

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();

            //validations            
            if (!isset($data['user_type']) || empty($data['user_type']) || !in_array($data['user_type'], array('1', '2'))) {
                $message = __('Please enter user type.');
            } else if (!isset($data['email']) || empty($data['email'])) {
                $message = __('Please enter email.');
            } else if (!isset($data['password']) || empty($data['password'])) {
                $message = __('Please enter password.');
            } else if (!isset($data['device']) || empty($data['device']) || !in_array($data['device'], array('ios', 'android'))) {
                $message = __('Please enter device type.');
            } else if (!isset($data['device_token']) || empty($data['device_token'])) {
                $message = __('Please enter device token.');
            }

            if (empty($message)) {

                $this->loadModel('Users');
                $user = $this->checkUser($data);

                if (!empty($user)) {

                    $status = 1;
                    $message = '';

                    $user['device'] = $data['device'];
                    $user['device_token'] = $data['device_token'];

                    if (empty($user['access_token'])) {
                        $user['access_token'] = md5(time());
                    }

                    $user['image_url'] = '';
                    $user['is_login'] = 1; // for driver
                    $user['last_login'] = DATABASE_FORMAT_CURRENT_DATE_TIME;
                    #$user['online_status'] = 1;

                    if (!empty($user['image'])) {
                        $user['image_url'] = $this->getImgUrl() . $user['image'];
                    }

                    $userData = $this->Users->newEntity($user);
                    $this->Users->save($userData);
                    
                    $info = $user;

                    $cardInfo = $this->getCustomerCards($user['id']);
                    if (!empty($cardInfo)) {
                        $cards = $cardInfo;
                    }
                } else {
                    $message = __('The email or password entered is incorrect.');
                }
            }
        }

        $response['status'] = $status;
        $response['message']= $message;
        $response['data']   = $info;
        $response['cards']  = $cards;

        $this->sendData($response);
    }

    /* 	
     * function name 	: logout
     * Author        	: Inderjit Singh
     * Date          	: Sep 14, 2017
     * input 		: {"id": "", "access_token": ""}
     */

    function logout() {

        $status = 0;

        if ($this->request->is('post')) {

            //get data
            $data = $this->getData();

            if (!isset($data['id']) || empty($data['id'])) {
                $message = __('Please enter user id.');
            }

            if (empty($message)) {

                $status = 1;
                $message = __('Logout successfully');

                $this->loadModel('Users');
                $user = $this->Users->find()
                        ->where(['Users.id' => $data['id']])
                        ->select(['id', 'access_token', 'device', 'device_token'])
                        ->first();

                if (!empty($user)) {
                    //$user->access_token = '';
                    $user->device = $user->device_token = '';
                    $user->is_login = 0;
                    $this->Users->save($user);
                }
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;

        $this->sendData($response);
    }

    /* 	
     * function name 	: register
     * Author        	: Inderjit Singh
     * Date          	: Sep 13, 2017
     * input 		: {"first_name": "Inderjit", "last_name": "Singh", "address": "E-37", "email": "inderjitsi@smartdatainc.net", "password": "123456", "phone": "(123) 456-7890", "device": "", "device_token": ""}
     */

    function register() {

        $status = 0;
        $message = '';
        $respData = $cards = json_decode("{}");

        if ($this->request->is('post')) {

            $data = sanitize_data($_POST); //$this->getData();

            $data['user_type_id'] = 1;
            $data['access_token'] = md5(time());
            $data['created'] = DATABASE_FORMAT_CURRENT_DATE_TIME;

            $this->loadModel('Users');
            $user = $this->Users->newEntity($data);


            if (!isset($data['device']) || empty($data['device'])) {

                $message = __('Please enter device type.');
            } else if (!isset($data['device_token']) || empty($data['device_token'])) {

                $message = __('Please enter device token.');
            } else if ($user->errors()) {

                $errors = $user->errors();
                $message = $this->getErrors($errors);

                #Log::write('error', "Resp Error: " . print_r($errors, true));
            } else {

                $data['image_url'] = '';
                $fkey = $data['device'] == 'ios' ? 0 : 'picture';

                $user->password = md5($data['password']);

                if (!empty($_FILES[$fkey]['name'])) {

                    $imgName = pathinfo($_FILES[$fkey]['name']);

                    $ext = strtolower($imgName['extension']);

                    if (in_array($ext, array('jpg', 'jpeg', 'png', 'gif'))) {

                        $destination = USER_IMG;
                        $destThumb = USER_IMG_THUMB;

                        $filename = time() . '-' . $_FILES[$fkey]['name'];
                        $file = $_FILES[$fkey];

                        $this->Upload->upload($file, $destThumb, $filename, array('type' => 'resizecrop', 'size' => array(200, 200), 'quality' => '100'));
                        $result = $this->Upload->upload($file, $destination, $filename, array());

                        //$resp = $this->Upload->upload_image($_FILES['picture']);                        

                        if ($result) {
                            $user->image = $data['image'] = $filename;
                        }
                    }
                }

                $this->Users->save($user);

                $userdata['id'] = $user->id;
                $respData = $this->checkUser($userdata);

                $respData['image_url'] = '';
                if (!empty($user->image)) {
                    $respData['image_url'] = $this->getImgUrl() . $user->image;
                }

                $cardInfo = $this->getCustomerCards($userdata['id']);
                if (!empty($cardInfo)) {
                    $cards = $cardInfo;
                }

                $status = 1;
                $message = __('Thank you for registering!');
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $respData;
        $response['cards'] = $cards;

        #Log::write('error', "Service Responce: " . print_r($response, true));

        $this->sendData($response);
    }

    /* 	
     * function name 	: forgot_password
     * Author        	: Inderjit Singh
     * Date          	: Sep 15, 2017
     * input 		: {"email": "inderjitsi@smartdatainc.net", "user_type": "1/2"}
     */

    function forgot_password() {

        $status = 0;
        $message = '';

        if ($this->request->is('post')) {

            $headers = getallheaders();

            //Get input parameters of web service
            $data = $this->getData();

            if (!isset($data['email']) || empty($data['email'])) {
                $message = __('Please enter email.');
            } elseif (!isset($data['user_type']) || empty($data['user_type']) || !in_array($data['user_type'], array('1', '2'))) {
                $message = __('Please select user type.');
            } else {

                $this->loadModel('Users');
                $user = $this->Users->find()
                        ->where(['Users.user_type_id' => $data['user_type'], 'Users.is_deleted' => 0, 'Users.email' => $data['email']])
                        ->select(['id', 'first_name', 'email'])
                        ->first();
                //prx($user);

                if (!empty($user)) {

                    $password = $this->Users->generate_password();

                    if ($this->Users->updateAll(['password' => md5($password)], ['id' => $user->id])) {

                        $locale = $this->request->session()->read('Config.language');

                        $emailTemplateTable = TableRegistry::get('EmailTemplate');
                        $emailTemplate = $emailTemplateTable->getEmailTemplate('forgotpassword');

                        $subject = $emailTemplate['subject'];
                        $description = $emailTemplate['description'];

                        if ($locale == 'es') {
                            $subject = $emailTemplate['subject_es'];
                            $description = $emailTemplate['description_es'];
                        }

                        $emailData['to'] = $user['email'];
                        $emailData['subject'] = $subject;
                        $emailData['template'] = str_replace(
                                ['{Name}', '{Password}'], [$user->first_name, $password], $description
                        );

                        $this->SendEmail->sendEmail($emailData);

                        $status = 1;
                        $message = __('New password has been sent to your email.');
                    }
                } else {
                    $message = __('User does not exists.');
                }
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;

        $this->sendData($response);
    }

    private function getImgUrl() {
        $url = BASE_URL . 'img/' . USER_PATH;
        return $url;
    }
    
    private function getIconUrl() {
        $url = BASE_URL . 'img/' . TANK_PATH;
        return $url;
    }

    /* 	
     * function name 	: basic_fare
     * Author        	: Inderjit Singh
     * Date          	: Oct 11, 2017
     * input 		: 
     */

    function basic_fare() {

        $this->loadModel('Taxes');
        $data = $this->Taxes->find()
                ->hydrate(false)
                ->first();

        $resp['status'] = 1;
        $resp['data'] = $data;

        $this->sendData($resp);
    }
    
    /* 	
     * function name 	: getTanks
     * Author        	: Inderjit Singh
     * Date          	: Oct 11, 2017
     * input 		: 
     */

    function getTanks() {

        $this->loadModel('Tanks');
        $this->loadModel('Taxes');
        
        $data = $this->Tanks->find()
                ->hydrate(false)
                ->where(['status' => 1, 'is_deleted' => 0])
                ->select(['id','category_id','name','name_es','price','icon','sequence'])
                ->order(['sequence' => 'ASC'])
                ->toArray();
        
        $info['Tank'] = $info['Stationary'] = [];
        
        $order_types = Configure::read('ORDER_TYPES');
        
        if(!empty($data)){
            foreach($data as $tank){
                
                $tank['icon_url'] = '';
                if(!empty($tank['icon']) && file_exists(TANK_IMG . $tank['icon'])){
                    $tank['icon_url'] = $this->getIconUrl().$tank['icon'];
                }
                $info[$order_types[$tank['category_id']]][] = $tank;
            }
        }
        //prx($info);
        
        $info['BasicFare'] = $this->Taxes->find()
                                            ->hydrate(false)
                                            ->first();
        
        $resp['status'] = 1;
        $resp['data'] = $info;

        $this->sendData($resp);
    }    

    /* 	
     * function name 	: checkout
     * Author        	: Inderjit Singh
     * Date          	: Sep 18, 2017
     * input 		: {"user_id":"28","order_type": "1/2", "delivery_address": "", "latitude": "", "longitude": "", "tank_id": "", "quantity": "", "delivery_date":"", "payment_method": "cash/cc"}
     */

    function checkout() {

        $status = $order_id = 0;
        $message = '';

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();            

            $message = $this->validateOrder($data);

            if (empty($message)) {

                $this->loadModel('Users');
                $driver = $this->Users->getNearestDriver($data);

                if (!empty($driver)) {

                    $this->loadModel('Orders');

                    $driver = $driver[0];
                    $order_id = $this->Orders->saveOrder($data, $driver);

                    $status = 1;
                    $message = __("Order has been placed, We will notify you once driver confirm the order.");
                } else {
                    $message = __('There are no drivers currently available please try again later.');
                }
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;
        $response['order_id'] = $order_id;

        $this->sendData($response);
    }

    function validateOrder($data = null) {

        $message = '';

        if (!isset($data['order_type']) || !in_array($data['order_type'], array('1', '2'))) {

            $message = __('Please enter order type.');
            
        } else if (!isset($data['delivery_address']) || empty($data['delivery_address'])) {

            $message = __('Please enter delivery address.');
            
        } else if (!isset($data['latitude']) || empty($data['latitude'])) {

            $message = __('Please enter latitude.');
            
        } else if (!isset($data['longitude']) || empty($data['longitude'])) {

            $message = __('Please enter longitude.');
            
        } else if (!isset($data['tank_id']) || empty($data['tank_id']) ) {
            
            $message = __('Please select tank.');
            
        } else if (!empty($data['tank_id']) ) {
            
            $this->loadModel('Tanks');
            $checkTank = $this->Tanks->find()
                                        ->where(['id' => $data['tank_id'], 'status' => 1, 'is_deleted' => 0])
                                        ->count();
            if($checkTank == 0){
                $message = __('Selected tank is not available.');
            }
            
        } else if (!isset($data['quantity']) || empty($data['quantity'])) {

            $message = __('Please enter quantity.');
            
        } else if (!isset($data['delivery_date']) || empty($data['delivery_date'])) {

            $message = __('Please enter delivery date.');
            
        } else if (!isset($data['payment_method']) || empty($data['payment_method']) || !in_array($data['payment_method'], array('cash', 'cc'))) {
            $message = __('Please select payment method.');
            
        } else if (!empty($data['payment_method']) && $data['payment_method'] == 'cc') {

            if (!isset($data['card_id']) || empty($data['card_id'])) {
                $message = __('Please enter card id.');
            } else {

                $this->loadModel('CustomerCreditCards');
                $checkCard = $this->getCustomerCards($data['user_id'], $data['card_id']);

                if (empty($checkCard)) {
                    $message = __('Customer credit card not found.');
                }
            }
        }

        return $message;
    }
    
    /* 	
     * function name 	: changeOrderStatus - Order Accepted or Rejected
     * Author        	: Inderjit Singh
     * Date          	: Sep 22, 2017
     * input 		: {"driver_id":"28","order_id":"2","status" : "0/1/2/3", "reason": ""}
     * extra params     : {"payment_method": "", "quantity": ""}
     */

    function changeOrderStatus() {

        $status = 0;
        $message = '';

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();

            if (!isset($data['driver_id']) || empty($data['driver_id'])) {
                
                $message = __('Please enter driver id.');
                
            } else if (!isset($data['order_id']) || empty($data['order_id'])) {
                
                $message = __('Please enter order id.');
                
            } else if (!isset($data['status']) || $data['status'] == '') {
                
                $message = __('Please enter status.');
                
            }else if ($data['status'] != '' && $data['status'] == 3) {
                
                if (!isset($data['payment_method']) || empty($data['payment_method'])) {                
                    $message = __('Please enter payment method.');
                }
                else if (!isset($data['quantity']) || empty($data['quantity'])) {                
                    $message = __('Please enter quantity.');
                }
            }

            if (empty($message)) {

                $this->loadModel('Orders');
                $order = $order_arr = $this->Orders->find()
                                        ->where(['Orders.id' => $data['order_id']])
                                        ->select(['id', 'tank_id','status']);
                
                $order = $order->first();
                $order_arr = $order_arr->hydrate(false)->first();                

                if ($order) {
                    
                    $up_status = $data['status'];

                    if ($data['status'] == 0) { // Reject case //send notification to other neareast driver
                        
                        $status = 1;
                        
                        $order->driver_id       = '';
                        
                        $this->rejectOrderCase($data['order_id']);
                        $message = __('Order has been rejected.');
                        
                    } else if ($data['status'] == 1) { // accept case
                        
                        if ($order->status == 0) {
                            
                            $check_in_progress_orders = $this->checkDriverInProgressOrders($data['driver_id']);    
                            
                            if($check_in_progress_orders == 0){
                            
                                $status = 1;
                                $up_status = 2;
                                $order->driver_id = $data['driver_id'];

                                $this->Orders->accpetOrderCase($data['order_id'], $data['driver_id']);
                                $message = __('Order has been accepted.');
                                
                            } else {
                                
                                $message = __('You cannot accept this order. Please complete previous order first.');              
                            }
                            
                        } else {
                            $status = 0;
                            $message = __('Order has already accepted by other driver.');
                        }
                        
                    } else if ($data['status'] == 2) { // out for delivery
                        
                        $status = 1;
                        $order->driver_id = $data['driver_id'];

                        $this->Orders->outDeliveryOrderCase($data['order_id']);
                        $message = __('Order has been marked as out for delivery.');
                        
                    } else if ($data['status'] == 3) { // mark as complete
                        
                        $order->driver_id       = $data['driver_id'];
                        
                        $order_arr['quantity']      = $data['quantity'];
                        $order_arr['payment_method']= $data['payment_method'];
                                
                        $this->Orders->saveOrder($order_arr); // calc order details
                        
                        $resp = $this->Orders->completeOrderCase($data['order_id']);

                        if ($resp['status'] == 0) {
                            $status = 0;
                            $message = $resp['message'];
                        } else {
                            $status = 1;
                            $message = __('Order has been marked as delivered.');
                        }
                    }

                    if ($status == 1) {

                        $this->loadModel('OrderLogs');
                        $data['created'] = DATABASE_FORMAT_CURRENT_DATE_TIME;
                        $orderLog = $this->OrderLogs->newEntity($data);

                        $this->OrderLogs->save($orderLog);

                        $order->status = $up_status;
                        $this->Orders->save($order);
                    }
                } else {
                    $message = __('Order does not exist.');
                }
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $this->sendData($response);
    }

    /* 	
     * function name 	: orderRejectCase
     * Author        	: Inderjit Singh
     * Date          	: Sep 26, 2017
     * params 		: order_id
     */

    function rejectOrderCase($order_id = null) {

        if (!empty($order_id)) {

            $this->loadModel('Orders');
            $this->loadModel('Users');

            $order = $this->Orders->find()
                    ->hydrate(false)
                    ->where(['Orders.id' => $order_id])
                    ->contain(['Users' => ['fields' => ['id', 'user_type_id', 'email', 'device', 'device_token']]])
                    ->first();

            if (!empty($order) && $order['status'] == 0) { // if order is pending
                $this->loadModel('OrderLogs');
                $driverId = $this->OrderLogs->find('list', ['keyField' => 'id', 'valueField' => 'driver_id'])
                        ->where(['order_id' => $order_id, 'status' => 0])
                        ->group(['driver_id'])
                        ->toArray();

                $order['not_drivers'] = $driverId;
                $driver = $this->Users->getNearestDriver($order);

                if (!empty($driver)) {

                    $driver = $driver[0];
                    $order_id = $this->Orders->saveOrder($order, $driver);
                    
                } else {

                    // send notification to customer driver not avaiable
                    $newData['order_id'] = $order['id'];
                    $newData['device'] = $order['user']['device'];
                    $newData['device_token'] = $order['user']['device_token'];
                    $newData['user_id'] = $order['user']['id'];
                    $newData['type'] = 'driver_not_available';
                    $newData['message'] = __('There are no drivers currently available please try again later.');
                    $newData['to'] = 'user';
                    //prx($newData);
                    #$this->Orders->send_notificatin($newData);
                }
            }
        }
    }    
    
    private function checkDriverInProgressOrders($driver_id = null){
        
        $check = 0;
        
        if(!empty($driver_id)){
            
            $this->loadModel('Orders');
            
            $count = $this->Orders->find()
                                    ->where(['Orders.driver_id' => $driver_id, 'Orders.status IN' => [1,2], 'Orders.is_deleted' => 0 ])
                                    ->count();
            if($count > 0){
                $check = 1;
            }
        }        
        return $check;
    }


    /* 	
     * function name 	: changeDriverOnlineStatus
     * Author        	: Aarti
     * Date          	: Sep 19, 2017
     * input 		: {"id": "28","status":"0/1"}
     */

    function changeDriverOnlineStatus() {

        $status = 0;
        $message = '';

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();

            if (!isset($data['status']) || !in_array($data['status'], array('0', '1'))) {
                $message = __('Please enter online status.');
            } else if (!isset($data['id']) || empty($data['id'])) {
                $message = __('Please enter id.');
            }

            if (empty($message)) {

                $this->loadModel('Users');
                $user = $this->checkUser($data);

                if ($user) {
                    
                    $online_date_time = DATABASE_FORMAT_CURRENT_DATE_TIME;
                    
                    $up_arr = ['online_status' => $data['status']];
                    
                    
                    if($data['status'] == 1){
                        
                        $up_arr = array_merge($up_arr,['online_date_time' => $online_date_time]);
                        
                    } else {                        
                        
                        $user['online_date_time'] = date(DATABASE_FORMAT_CURRENT_DATE_TIME, strtotime($user['online_date_time']));
                        
                        $date_a = new \DateTime($user['online_date_time']);
                        $date_b = new \DateTime($online_date_time);

                        $interval = date_diff($date_a,$date_b);                        
                        //prx($interval);   
                        
                        $time = $interval->format('%h:%i');
                        $time2 = $user['online_hours'];

                        $online_hours = date("H:i",strtotime($time)+strtotime($time2));                        
                        $up_arr = array_merge($up_arr,['online_hours' => $online_hours]);
                    }
                    
                    $this->Users->updateAll($up_arr, ['id' => $data['id']]);
                    $status = 1;
                    $message = __('Your status have been updated successfully.');
                    
                } else {
                    $message = __('User does not exist.');
                }
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $response['online_status'] = (int) $data['status'];
        $this->sendData($response);
    }

    /* 	
     * function name 	: changePassword
     * Author        	: Aarti
     * Date          	: Sep 19, 2017
     * input 		: {"id": "","current_password":"","new_password":""}
     */

    function changePassword() {

        $status = 0;
        $message = '';

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();

            if (!isset($data['current_password']) || empty($data['current_password'])) {

                $message = __('Please enter current password.');
            } else if (!isset($data['new_password']) || empty($data['new_password'])) {

                $message = __('Please enter new password.');
            } /* elseif ($data['new_password'] == $data['current_password']) {
              $message = __('Old password and new password should not be same.');
              } */

            if (empty($message)) {

                $this->loadModel('Users');

                $data['password'] = $data['current_password'];
                $data['hydrate'] = true;

                $user = $this->checkUser($data);

                if ($user) {

                    $user->password = md5($data['new_password']);
                    $this->Users->save($user);

                    $status = 1;
                    $message = __('You have successfully update the password.');
                } else {
                    $message = __('Current password not match.');
                }
            }

            $response['status'] = $status;
            $response['message'] = $message;
            $this->sendData($response);
        }
    }

    /* 	
     * function name 	: updateProfile
     * Author        	: Aarti
     * Date          	: Sep 19, 2017
     * input 		: {"id": "28","device":"ios/andriod","user_type":"1/2", "first_name": "Aarti",'last_name':'Shah','address':'Zirakpur', 'email': '', 'phone':'(555)123 5656', 'picture': ''}
     */

    function updateProfile() {

        $status = 0;
        $message = '';
        $user = json_decode("{}");

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = sanitize_data($_POST);

            if (!isset($data['id']) || empty($data['id'])) {
                $message = __('Please enter id.');
            } else if (!isset($data['user_type']) || empty($data['user_type']) || !in_array($data['user_type'], array('1', '2'))) {
                $message = __('Please enter user type.');
            } else if (!isset($data['first_name']) || empty($data['first_name'])) {
                $message = __('Please enter first name.');
            } else if (!isset($data['email']) || empty($data['email'])) {
                $message = __('Please enter email.');
            } else if (!isset($data['address']) || empty($data['address'])) {
                $message = __('Please enter address.');
            }

            if (empty($message)) {

                $this->loadModel('Users');
                $user = $this->checkUser($data);

                if ($user) {

                    $user['first_name'] = $data['first_name'];
                    $user['last_name'] = $data['last_name'];
                    $user['email'] = $data['email'];
                    $user['phone'] = $data['phone'];
                    $user['address'] = $data['address'];
                    $user['image_url'] = !empty($user['image']) ? $this->getImgUrl() . $user['image'] : '';

                    $userData = $this->Users->newEntity($user);

                    if ($userData->errors()) {

                        $user = '';
                        $errors = $userData->errors();
                        $message = $this->getErrors($errors);
                    } else {

                        $fkey = $data['device'] == 'ios' ? 0 : 'picture';

                        if (!empty($_FILES[$fkey]['name'])) {

                            $imgName = pathinfo($_FILES[$fkey]['name']);
                            $ext = strtolower($imgName['extension']);

                            if (in_array($ext, array('jpg', 'jpeg', 'png', 'gif'))) {

                                $destination = USER_IMG;
                                $destThumb = USER_IMG_THUMB;

                                $filename = time() . '-' . $_FILES[$fkey]['name'];
                                $file = $_FILES[$fkey];

                                $this->Upload->upload($file, $destThumb, $filename, array('type' => 'resizecrop', 'size' => array(200, 200), 'quality' => '100'));
                                $result = $this->Upload->upload($file, $destination, $filename, array());

                                if ($result) {

                                    //remove old image
                                    if (!empty($user['image'])) {
                                        @unlink($destination . $user['image']);
                                        @unlink($destThumb . $user['image']);
                                    }

                                    $user['image'] = $filename;
                                    $user['image_url'] = $this->getImgUrl() . $user['image'];
                                }
                            }
                        }

                        $userData = $this->Users->newEntity($user);
                        $this->Users->save($userData);

                        $status = 1;
                        $message = __('You profile details has been updated.');
                    }
                } else {
                    $message = __('User does not exist.');
                }
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $user;

        $this->sendData($response);
    }

    /* 	
     * function name 	: updateLatLong
     * Author        	: Aarti
     * Date          	: Sep 20, 2017
     * input 		: {"id": "28","latitude":"40.71727401","longitude":"-74.00898606"}
     */

    function updateLatLong() {

        $status = 0;
        $message = '';

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();

            if (!isset($data['id']) || empty($data['id'])) {
                $message = __('Please enter id.');
            } else if (!isset($data['latitude']) || empty($data['latitude'])) {
                $message = __('Please enter latitude.');
            } else if (!isset($data['longitude']) || empty($data['longitude'])) {
                $message = __('Please enter longitude.');
            }

            if (empty($message)) {

                $this->loadModel('Users');
                $user = $this->Users->find()
                        ->where(['Users.id' => $data['id']])
                        ->count();

                if ($user) {
                    $this->Users->updateAll(['latitude' => $data['latitude'], 'longitude' => $data['longitude']], ['id' => $data['id']]);
                    $status = 1;
                    $message = __('You have successfully update the lat and long.');
                } else {
                    $message = __('User does not exist.');
                }
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;
        $this->sendData($response);
    }

    /* 	
     * function name 	: addRating
     * Author        	: Aarti
     * Date          	: Sep 20, 2017
     * input 		: { "user_id":"28","order_id":"2","driver_id":"1","star_rating" :5,"feedback" : "Well done!!" }
     */

    function addRating() {

        $statusCode = 0;
        $message = '';

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();

            if (!isset($data['user_id']) || empty($data['user_id'])) {
                $message = __('Please enter user id.');
            } else if (!isset($data['order_id']) || empty($data['order_id'])) {
                $message = __('Please enter order id.');
            } else if (!isset($data['driver_id']) || empty($data['driver_id'])) {
                $message = __('Please enter driver id.');
            } else if (!isset($data['feedback']) || empty($data['feedback'])) {
                //$message = __('Please enter feedback.');
            } else if (!isset($data['star_rating']) || empty($data['star_rating'])) {
                $message = __('Please enter star rating.');
            }

            if (empty($message)) {

                $this->loadModel('Users');
                $user = $this->Users->find()
                        ->where(['Users.id' => $data['user_id']])
                        ->count();

                if ($user) {

                    $this->loadModel('Ratings');
                    $ratingExists = $this->Ratings->find()
                            ->where(['user_id' => $data['user_id'], 'order_id' => $data['order_id'], 'driver_id' => $data['driver_id']])
                            ->first();

                    if (!empty($ratingExists)) {

                        $data['modified'] = DATABASE_FORMAT_CURRENT_DATE_TIME;

                        $this->Ratings->updateAll($data, ['id' => $ratingExists['id']]);

                        $statusCode = 1;
                        $message = __('Review updated successfully.');
                    } else {

                        $data['created'] = DATABASE_FORMAT_CURRENT_DATE_TIME;

                        $ratingData = $this->Ratings->newEntity($data);
                        if ($this->Ratings->save($ratingData)) {
                            $statusCode = 1;
                            $message = __('Review added successfully.');
                        } else {
                            $message = __('Error, Not saved.');
                        }
                    }
                } else {
                    $message = __('User does not exist.');
                }
            }
        }
        $response['status'] = $statusCode;
        $response['message'] = $message;
        $this->sendData($response);
    }

    /* 	
     * function name 	: addCreditCardDetails
     * Author        	: Aarti
     * Date          	: Sep 20, 2017
     * input 		: {"user_id": "28","card_id": "","stripe_token":""}
     */

    function addCreditCardDetails() {

        $status = 0;
        $message = '';
        $respData = json_decode("{}");

        $this->loadModel('Orders');
        $this->Orders->setStripeInit();

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();

            $this->loadModel('CustomerCreditCards');

            if (!isset($data['user_id']) || empty($data['user_id'])) {

                $message = __('Please enter user id.');
            } else if (!isset($data['stripe_token']) || empty($data['stripe_token'])) {

                $message = __('Please enter stripe token.');
            } else if (!empty($data['card_id'])) {

                $checkCard = $this->getCustomerCards($data['user_id'], $data['card_id']);

                if (!empty($checkCard)) {

                    $card['id'] = $checkCard['id'];

                    $this->loadModel('Orders');
                    $checkOrder = $this->Orders->find()
                            ->where(['user_id' => $data['user_id'], 'card_id' => $data['card_id'], 'payment_method' => 'cc', 'status' => 0, 'is_deleted' => 0])
                            ->count();

                    if ($checkOrder > 0) {
                        $message = __('Currently your card is associated with order which is in completed yet.');
                    }
                } else {
                    $message = __('Credit card not found.');
                }
            }

            if (empty($message)) {

                $this->loadModel('Users');
                $user = $this->Users->find()
                        ->where(['Users.id' => $data['user_id']])
                        ->select(['id', 'email', 'stripe_customer_id'])
                        ->first();

                //prx($user);

                if ($user) {

                    /* create token for testing */
                    /* $stripe_token = \Stripe\Token::create(array(
                      "card" => array(
                      "number" => "4242424242424242",
                      "exp_month" => 10,
                      "exp_year" => 2018,
                      "cvc" => "123"
                      )
                      ));
                      echo $token = $stripe_token->id; die; */

                    $token = $data['stripe_token'];

                    if (!$user['stripe_customer_id']) {
                        // Create a Customer on Stripe
                        try {
                            $customers = \Stripe\Customer::create(array(
                                        "email" => $user['email'],
                                        "source" => $token // obtained with Stripe.js
                            ));
                            $customer = $customers->__toArray(true);
                        } catch (\Stripe\Error\ApiConnection $e) {
                            // Network problem, perhaps try again.
                            $e_json = $e->getJsonBody();
                            $message = $e_json['error']['message'];
                        } catch (\Stripe\Error\InvalidRequest $e) {
                            // You screwed up in your programming. Shouldn't happen!
                            $e_json = $e->getJsonBody();
                            $message = $e_json['error']['message'];
                        } catch (\Stripe\Error\Api $e) {

                            $e_json = $e->getJsonBody();
                            $message = $e_json['error']['message'];
                        } catch (\Stripe\Error\Card $e) {

                            $e_json = $e->getJsonBody();
                            $message = $e_json['error']['message'];
                            // Card was declined.
                        }
                    } else {

                        try {

                            $cu = \Stripe\Customer::retrieve($user['stripe_customer_id']);
                            $cu->source = $token; // obtained with Stripe.js                            
                            $cu->save();
                            $customer = $cu->__toArray(true);
                        } catch (\Stripe\Error\ApiConnection $e) {
                            // Network problem, perhaps try again.
                            $e_json = $e->getJsonBody();
                            $message = $e_json['error']['message'];
                        } catch (\Stripe\Error\InvalidRequest $e) {
                            // You screwed up in your programming. Shouldn't happen!
                            $e_json = $e->getJsonBody();
                            $message = $e_json['error']['message'];
                        } catch (\Stripe\Error\Api $e) {

                            $e_json = $e->getJsonBody();
                            $message = $e_json['error']['message'];
                        } catch (\Stripe\Error\Card $e) {

                            $e_json = $e->getJsonBody();
                            $message = $e_json['error']['message'];
                            // Card was declined.
                        }
                    }

                    if (empty($message)) {
                        //prx($customer);
                        if ($customer && !isset($customer['error'])) {

                            if (!$user['stripe_customer_id']) {
                                $user->stripe_customer_id = $customer['id'];
                                $this->Users->save($user);
                            }

                            if (empty($card['id'])) {
                                $card['created'] = DATABASE_FORMAT_CURRENT_DATE_TIME;
                            }

                            $card['srtipe_card_id'] = $customer['sources']['data'][0]['id'];
                            $card['name'] = $customer['sources']['data'][0]['name'];
                            $card['object'] = $customer['sources']['data'][0]['object'];
                            $card['brand'] = $customer['sources']['data'][0]['brand'];
                            $card['exp_month'] = $customer['sources']['data'][0]['exp_month'];
                            $card['exp_year'] = $customer['sources']['data'][0]['exp_year'];
                            $card['funding'] = $customer['sources']['data'][0]['funding'];
                            $card['last4'] = $customer['sources']['data'][0]['last4'];
                            $card['user_id'] = $user->id;

                            $respData = $card;

                            $card = $this->CustomerCreditCards->newEntity($card);
                            $this->CustomerCreditCards->save($card);

                            $respData['id'] = $card->id;

                            $status = 1;
                            $message = __("Credit card information has been saved successfully.");
                        } else {
                            $message = $customer['error']['message'];
                        }
                    }
                } else {
                    $message = __('User does not exist.');
                }
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $respData;

        $this->sendData($response);
    }
    
    /* 	
     * function name 	: addCard
     * Author        	: Inderjit
     * Date          	: Oct 23, 2017
     * input 		: {"user_id": "28","card_id": "","holder_name":"", "card_number": "", "cvv2": "", "expiration_month": "", "expiration_year": ""}
     */

    function addCard() {

        $status = 0;
        $message = '';
        $respData = json_decode("{}");

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();
            //prx($data);
            
            $message = $this->validateCard($data);                        

            if (empty($message)) {
                
                $this->loadModel('Users');
                $this->loadModel('CustomerCreditCards');
                
                $user = $this->Users->find()
                        ->where(['Users.id' => $data['user_id']])
                        ->select(['id', 'email'])
                        ->first();

                //prx($user);

                if ($user) {
                    
                    $openpay = $this->OpenPayInit();
                    //prx($openpay);
                    
                    try {
                        
                        $cardData = $data;
                        unset($cardData['user_id'],$cardData['card_id']);
                        
                        $card = $openpay->cards->add($cardData);
                        prx($card);
                        
                    } catch (\OpenpayApiTransactionError $e) {                                
                        $message = $e->getMessage();
                    } catch (OpenpayApiRequestError $e) {
                        $message = $e->getMessage();
                    } catch (\OpenpayApiConnectionError $e) {
                        $message = $e->getMessage();
                    } catch (\OpenpayApiAuthError $e) {
                        $message = $e->getMessage();
                    } catch (\OpenpayApiError $e) {
                        $message = $e->getMessage();
                    } catch (Exception $e) {
                        $message = $e->getMessage();
                    }
                    
                    prx($message);

                    if (empty($message)) {
                        
                        if ($card) {                           
                            $status = 1;
                            $message = __("Credit card information has been saved successfully.");
                        } else {
                            $message = $customer['error']['message'];
                        }
                    }
                } else {
                    $message = __('User does not exist.');
                }
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $respData;

        $this->sendData($response);
    }
    
    private function validateCard($data=null){
        
        $message = '';
        
        if (!isset($data['user_id']) || empty($data['user_id'])) {
                
            $message = __('Please enter user id.');

        } else if (!isset($data['holder_name']) || empty($data['holder_name'])) {

            $message = __('Please enter holder name.');

        }else if (!isset($data['card_number']) || empty($data['card_number'])) {

            $message = __('Please enter card number.');

        }else if (!isset($data['cvv2']) || empty($data['cvv2'])) {

            $message = __('Please enter cvv.');

        }else if (!isset($data['expiration_month']) || empty($data['expiration_month'])) {

            $message = __('Please enter expiration month.');

        }
        else if (!isset($data['expiration_year']) || empty($data['expiration_year'])) {

            $message = __('Please enter expiration year.');

        } else if (!empty($data['card_id'])) {

            $checkCard = $this->getCustomerCards($data['user_id'], $data['card_id']);

            if (!empty($checkCard)) {

                $card['id'] = $checkCard['id'];

                $this->loadModel('Orders');
                $checkOrder = $this->Orders->find()
                        ->where(['user_id' => $data['user_id'], 'card_id' => $data['card_id'], 'payment_method' => 'cc', 'status' => 0, 'is_deleted' => 0])
                        ->count();

                if ($checkOrder > 0) {
                    $message = __('Currently your card is associated with order which is in completed yet.');
                }
            } else {
                $message = __('Credit card not found.');
            }
        }
        return $message;        
    }
    
    private function OpenPayInit(){
        
        \Openpay::setProductionMode(OPENPAY_MODE);
        $openpay = \Openpay::getInstance(OPENPAY_ID, OPENPAY_PUBLIC_KEY);
        
        return $openpay;
    }


    /* 	
     * function name 	: getOrderHistory - Order History of a customer
     * Author        	: Anu Gupta
     * Date          	: Sep 25, 2017
     * input 		: {"user_id":"28"}
     */

    function getOrderHistory() {
        
        $status = 0;
        $message = '';

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();            

            //validations            
            if (!isset($data['user_id']) || empty($data['user_id'])) {
                $message = __('Please enter user id.');
            }

            if (empty($message)) {

                $this->loadModel('Orders');

                $info = [];
                $currentDate = date('Y-m-d');
                $where = ['Orders.user_id' => $data['user_id'], 'Orders.is_deleted' => 0];
                
                //pending / hold orders
                $wherePending = array_merge($where, ['Orders.status' => 0]);
                
                //out for delivery
                $whereCurrent = array_merge($where, ['Orders.status' => 2]);

                //previous date or complemted
                //$wherePrevious = array_merge($where, ['OR' => ['Orders.delivery_date < ' => $currentDate, 'Orders.status' => 3]]);   
                $wherePrevious = array_merge($where, ['Orders.status' => 3]);

                //today / future orders and (pending / confirm)
                //$whereUpcoming = array_merge($where, ['Orders.delivery_date >= ' => $currentDate, 'Orders.status IN' => [0, 1]]);  

                //cancelled order
                $whereCancel = array_merge($where, ['Orders.status' => 4]);                

                $contain = [
                    'Driver'    => ['fields' => ['id', 'first_name', 'last_name']],
                    'Ratings'   => ['fields' => ['id', 'star_rating']],
                    'Tanks'      => ['fields' => ['id', 'name', 'name_es', 'icon']],
                ];

                $order = ['Orders.delivery_date DESC'];
                $group = ['Orders.id'];
                
                $info['PendingOrders'] = $this->Orders->find()
                        ->hydrate(false)
                        ->where($wherePending)
                        ->contain($contain)
                        ->order($order)
                        ->group($group)
                        ->toArray();
                
                $info['CurrentOrders'] = $this->Orders->find()
                        ->hydrate(false)
                        ->where($whereCurrent)
                        ->contain($contain)
                        ->order($order)
                        ->group($group)
                        ->toArray();

                $info['PreviousOrders'] = $this->Orders->find()
                        ->hydrate(false)
                        ->where($wherePrevious)
                        ->contain($contain)
                        ->group($group)
                        ->order($order)
                        ->toArray();

                /*$info['UpcomingOrders'] = $this->Orders->find()
                        ->hydrate(false)
                        ->where($whereUpcoming)
                        ->contain($contain)
                        ->order($order)
                        ->group($group)
                        ->toArray();*/

                $info['CancelledOrders'] = $this->Orders->find()
                        ->hydrate(false)
                        ->where($whereCancel)
                        ->contain($contain)
                        ->order($order)
                        ->group($group)
                        ->toArray();                
                
                $info = $this->updateKeyFields($info);
                

                $status = 1;
                $message = __('Success');
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $info;
        $this->sendData($response);
    }

    function updateKeyFields($info = null) {
        
        $driver = json_decode("{}");
        
        if (!empty($info)) {
            
            //$order_size_val = Configure::read('ORDER_SIZE_VAL');
            //$order_status = Configure::read('ORDER_STATUS');
            
            $order_types = Configure::read('ORDER_TYPES');
            $payment_methods = Configure::read('PAYMENT_METHODS');
            
            if(!empty($info['PendingOrders'])){
            
                foreach ($info['PendingOrders'] as $coKey => $corder) {
                    
                    $info['PendingOrders'][$coKey]['order_type_val'] = __($order_types[$corder['order_type']]);
                    $info['PendingOrders'][$coKey]['delivery_date'] = date('Y-m-d', strtotime($corder['delivery_date']));
                    $info['PendingOrders'][$coKey]['total'] = number_format($corder['total'], 2);
                    $info['PendingOrders'][$coKey]['payment_method_val'] = __($payment_methods[$corder['payment_method']]);
                    
                    if(empty($info['PendingOrders'][$coKey]['driver_id'])){
                        $info['PendingOrders'][$coKey]['driver_id'] = 0;
                    }
                    
                    if (!isset($info['PendingOrders'][$coKey]['driver']) || empty($info['PendingOrders'][$coKey]['driver'])) {
                        $info['PendingOrders'][$coKey]['driver'] = $driver;
                    }

                    $info['PendingOrders'][$coKey]['rating'] = !empty($info['PendingOrders'][$coKey]['rating']['star_rating']) ? 1 : 0;
                    
                    $info['PendingOrders'][$coKey]['tank']['icon_url'] = '';
                    if(!empty($info['PendingOrders'][$coKey]['tank']['icon']) && file_exists(TANK_IMG . $info['PendingOrders'][$coKey]['tank']['icon'])){
                        $info['PendingOrders'][$coKey]['tank']['icon_url'] = $this->getIconUrl().$info['PendingOrders'][$coKey]['tank']['icon'];
                    }                    
                }
            }
            
            if(!empty($info['CurrentOrders'])){
            
                foreach ($info['CurrentOrders'] as $coKey => $corder) {

                    $info['CurrentOrders'][$coKey]['order_type_val'] = __($order_types[$corder['order_type']]);
                    //$info['CurrentOrders'][$coKey]['size_val'] = __($order_size_val[$corder['size']]);
                    $info['CurrentOrders'][$coKey]['delivery_date'] = date('Y-m-d', strtotime($corder['delivery_date']));
                    $info['CurrentOrders'][$coKey]['total'] = number_format($corder['total'], 2);
                    $info['CurrentOrders'][$coKey]['payment_method_val'] = __($payment_methods[$corder['payment_method']]);
                    
                    if(empty($info['CurrentOrders'][$coKey]['driver_id'])){
                        $info['CurrentOrders'][$coKey]['driver_id'] = 0;
                    }
                    
                    if (!isset($info['CurrentOrders'][$coKey]['driver']) || empty($info['CurrentOrders'][$coKey]['driver'])) {
                        $info['CurrentOrders'][$coKey]['driver'] = $driver;
                    }

                    $info['CurrentOrders'][$coKey]['rating'] = !empty($info['CurrentOrders'][$coKey]['rating']['star_rating']) ? 1 : 0;
                    
                    $info['CurrentOrders'][$coKey]['tank']['icon_url'] = '';
                    if(!empty($info['CurrentOrders'][$coKey]['tank']['icon']) && file_exists(TANK_IMG . $info['CurrentOrders'][$coKey]['tank']['icon'])){
                        $info['CurrentOrders'][$coKey]['tank']['icon_url'] = $this->getIconUrl().$info['CurrentOrders'][$coKey]['tank']['icon'];
                    }                    
                }
            }
            
            if(!empty($info['PreviousOrders'])){

                foreach ($info['PreviousOrders'] as $coKey => $corder) {

                    $info['PreviousOrders'][$coKey]['order_type_val'] = __($order_types[$corder['order_type']]);
                    //$info['PreviousOrders'][$coKey]['size_val'] = __($order_size_val[$corder['size']]);
                    $info['PreviousOrders'][$coKey]['delivery_date'] = date('Y-m-d', strtotime($corder['delivery_date']));
                    $info['PreviousOrders'][$coKey]['total'] = number_format($corder['total'], 2);
                    $info['PreviousOrders'][$coKey]['payment_method_val'] = __($payment_methods[$corder['payment_method']]);
                    
                    if(empty($info['PreviousOrders'][$coKey]['driver_id'])){
                        $info['PreviousOrders'][$coKey]['driver_id'] = 0;
                    }
                    
                    if (!isset($info['PreviousOrders'][$coKey]['driver']) || empty($info['PreviousOrders'][$coKey]['driver'])) {
                        $info['PreviousOrders'][$coKey]['driver'] = $driver;
                    }

                    $info['PreviousOrders'][$coKey]['rating'] = !empty($info['PreviousOrders'][$coKey]['rating']['star_rating']) ? 1 : 0;
                    $info['PreviousOrders'][$coKey]['tank']['icon_url'] = '';
                    if(!empty($info['PreviousOrders'][$coKey]['tank']['icon']) && file_exists(TANK_IMG . $info['PreviousOrders'][$coKey]['tank']['icon'])){
                        $info['PreviousOrders'][$coKey]['tank']['icon_url'] = $this->getIconUrl().$info['PreviousOrders'][$coKey]['tank']['icon'];
                    }
                }
            }
            
            if(!empty($info['UpcomingOrders'])){

                foreach ($info['UpcomingOrders'] as $coKey => $corder) {

                    $info['UpcomingOrders'][$coKey]['order_type_val'] = __($order_types[$corder['order_type']]);
                    //$info['UpcomingOrders'][$coKey]['size_val'] = __($order_size_val[$corder['size']]);
                    $info['UpcomingOrders'][$coKey]['delivery_date'] = date('Y-m-d', strtotime($corder['delivery_date']));
                    $info['UpcomingOrders'][$coKey]['total'] = number_format($corder['total'], 2);
                    $info['UpcomingOrders'][$coKey]['payment_method_val'] = __($payment_methods[$corder['payment_method']]);
                    
                    if(empty($info['UpcomingOrders'][$coKey]['driver_id'])){
                        $info['UpcomingOrders'][$coKey]['driver_id'] = 0;
                    }
                    
                    if (!isset($info['UpcomingOrders'][$coKey]['driver']) || empty($info['UpcomingOrders'][$coKey]['driver'])) {
                        $info['UpcomingOrders'][$coKey]['driver'] = $driver;
                    }

                    $info['UpcomingOrders'][$coKey]['rating'] = !empty($info['UpcomingOrders'][$coKey]['rating']['star_rating']) ? 1 : 0;
                    $info['UpcomingOrders'][$coKey]['tank']['icon_url'] = '';
                    if(!empty($info['UpcomingOrders'][$coKey]['tank']['icon']) && file_exists(TANK_IMG . $info['UpcomingOrders'][$coKey]['tank']['icon'])){
                        $info['UpcomingOrders'][$coKey]['tank']['icon_url'] = $this->getIconUrl().$info['UpcomingOrders'][$coKey]['tank']['icon'];
                    }
                }
            }
            
            if(!empty($info['CancelledOrders'])){
            
                foreach ($info['CancelledOrders'] as $coKey => $corder) {

                    $info['CancelledOrders'][$coKey]['order_type_val'] = __($order_types[$corder['order_type']]);
                    //$info['CancelledOrders'][$coKey]['size_val'] = __($order_size_val[$corder['size']]);
                    $info['CancelledOrders'][$coKey]['delivery_date'] = date('Y-m-d', strtotime($corder['delivery_date']));
                    $info['CancelledOrders'][$coKey]['total'] = number_format($corder['total'], 2);
                    $info['CancelledOrders'][$coKey]['payment_method_val'] = __($payment_methods[$corder['payment_method']]);
                    
                    if(empty($info['CancelledOrders'][$coKey]['driver_id'])){
                        $info['CancelledOrders'][$coKey]['driver_id'] = 0;
                    }
                    
                    if (!isset($info['CancelledOrders'][$coKey]['driver']) || empty($info['CancelledOrders'][$coKey]['driver'])) {
                        $info['CancelledOrders'][$coKey]['driver'] = $driver;
                    }

                    $info['CancelledOrders'][$coKey]['rating'] = !empty($info['CancelledOrders'][$coKey]['rating']['star_rating']) ? 1 : 0;
                    $info['CancelledOrders'][$coKey]['tank']['icon_url'] = '';
                    if(!empty($info['CancelledOrders'][$coKey]['tank']['icon']) && file_exists(TANK_IMG . $info['CancelledOrders'][$coKey]['tank']['icon'])){
                        $info['CancelledOrders'][$coKey]['tank']['icon_url'] = $this->getIconUrl().$info['CancelledOrders'][$coKey]['tank']['icon'];
                    }
                }
            }
        }

        return $info;
    }

    /* 	
     * function name 	: getUserInfo
     * Author        	: Inderjit
     * Date          	: Sep 27, 2017
     * input 		: {"user_id": "","user_type":"1/2"}
     */

    function getUserInfo() {

        $status = 0;
        $message = '';
        $user = json_decode("{}");

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();

            //validations            
            if (!isset($data['user_id']) || empty($data['user_id'])) {
                $message = __('Please enter user id.');
            } else if (!isset($data['user_type']) || empty($data['user_type'])) {
                $message = __('Please enter user type.');
            }

            if (empty($message)) {

                $data['id'] = $data['user_id'];
                $user = $this->checkUser($data);

                if (!empty($user)) {

                    $user['image_url'] = '';

                    if (!empty($user['image'])) {
                        $user['image_url'] = $this->getImgUrl() . $user['image'];
                    }

                    $status = 1;
                    $message = 'success';
                } else {
                    $message = 'User does not exists.';
                }
            }
        }

        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $user;
        $this->sendData($response);
    }

    /* 	
     * function name 	: cancelOrder - Order Canceled
     * Author        	: Kavita
     * Date          	: Sep 28, 2017
     * input 		: {"user_id", "order_id":"2"}
     */

    function cancelOrder() {

        $status = 0;
        $message = '';

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();

            if (!isset($data['user_id']) || empty($data['user_id'])) {
                $message = __('Please enter user id.');
            } else if (!isset($data['order_id']) || empty($data['order_id'])) {
                $message = __('Please enter order id.');
            }

            if (empty($message)) {

                $this->loadModel('Orders');
                $order = $this->Orders->find()
                        ->where(['Orders.id' => $data['order_id'], 'Orders.user_id' => $data['user_id']])
                        ->select(['id', 'status'])
                        ->first();

                if (!empty($order)) {

                    if ($order->status == 0) { // in pending stage                    
                        $status = 1;
                        $order->status = 4;

                        $this->Orders->save($order);
                        $message = __('Order has been canceled.');
                    } else {
                        $message = __('You cannot cancel this order.');
                    }
                } else {
                    $message = __('Order does not exist.');
                }
            }
        }
        $response['status'] = $status;
        $response['message'] = $message;
        $this->sendData($response);
    }      

    function getCustomerCards($user_id = null, $card_id = null) {

        $cards = '';
        if (!empty($user_id)) {

            $where = ['user_id' => $user_id];

            if (!empty($card_id)) {
                $where = array_merge($where, ['id' => $card_id]);
            }

            $this->loadModel('CustomerCreditCards');
            $cards = $this->CustomerCreditCards->find()
                    ->hydrate(false)
                    ->where($where)
                    ->first();
        }
        return $cards;
    }    
    
    /* 	
     * function name 	: order_details
     * Author        	: Inderjit
     * Date          	: Oct 23, 2017
     * Input            : {"driver_id": "", "order_id": ""}
     */

    function order_details() {
        
        $status = 0;
        $message = '';
        $info = json_decode("{}");

        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();            

            //validations            
            if (!isset($data['driver_id']) || empty($data['driver_id'])) {
                $message = __('Please enter driver id.');
            } else if (!isset($data['order_id']) || empty($data['order_id'])) {
                $message = __('Please enter order id.');
            }

            if (empty($message)) {
                
                $this->loadModel('Orders');
                $order = $this->Orders->find()
                                        ->hydrate(false)
                                        ->where(['Orders.status' => 2, 'Orders.driver_id' => $data['driver_id'], 'Orders.id' => $data['order_id'] ])
                                        ->contain(['Tanks'])
                                        ->first();
                if(!empty($order)){
                    
                    $info = $order;
                    
                    $status = 1;
                    $message = __('Success');
                    
                    $info['delivery_date'] = date('Y-m-d', strtotime($info['delivery_date']));     
                    
                } else {
                    
                    $message = __('Order does not exists.');                    
                }                
            }
        }
        
        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $info;
        $this->sendData($response); 
    }
    
    /* 	
     * function name 	: driver_orders
     * Author        	: Inderjit
     * Date          	: Oct 16, 2017
     * Input            : {"driver_id": ""}
     */

    function driver_orders() {
        
        $status = 0;
        $message = '';
        $info = [];
        
        if ($this->request->is('post')) {

            //Get input parameters of web service
            $data = $this->getData();

            if (!isset($data['driver_id']) || empty($data['driver_id'])) {
                $message = __('Please enter driver id.');
            }

            if (empty($message)) {                
                
                $status = 1;                                
                $this->loadModel('Orders');                
                
                $info = $this->getDriverOrders($data);
                
                if(empty($info['CurrentOrders']) && empty($info['PreviousOrders']) && empty($info['UpcomingOrders']) && empty($info['CancelledOrders'])){
                    $message = __('No record found.');
                } else {
                
                    $status = 1;
                    $message = __('Success');
                }
            }
        }
        
        $response['status'] = $status;
        $response['message'] = $message;
        $response['data'] = $info;
        $this->sendData($response); 
    }    
    
    private function getDriverOrders($data=null){
        
        $today = date('Y-m-d');
        $st_date = $today.' 00:00';
        $en_date = $today. '23 :59';        
        
        //$where = ['Orders.driver_id' => $data['driver_id'], 'Orders.is_deleted' => 0];
        $where = ['Orders.driver_id' => $data['driver_id']];

        //out for delivery
        //$whereCurrent = array_merge($where, ['Orders.status IN' => [1,2] ,'Orders.delivery_date >= ' => $st_date, 'Orders.delivery_date <=' => $en_date]);                
        $whereCurrent = array_merge($where, ['Orders.status IN' => [0,1,2] , 'Orders.delivery_date <=' => $en_date]);

        //previous date or complemted
        $wherePrevious = array_merge($where, ['Orders.status' => 3]);

        //upcoming 
        $whereUpcoming = array_merge($where, ['Orders.delivery_date > ' => $today, 'Orders.status IN' => [1,2] ]);

        //cancelled order
        $whereCancel = array_merge($where, ['Orders.status' => 4]);
        
        //only in case of driver revenue api
        if(!empty($data['start']) && !empty($data['end'])){
            
            $start  = $data['start'].' 00:00';
            $end    = $data['end'].' 23:59';
            
            $new_con = ['Orders.delivery_date >=' => $start, 'Orders.delivery_date <=' => $end];
            
            $whereCurrent = array_merge($whereCurrent,$new_con);
            
            $wherePrevious = array_merge($wherePrevious,$new_con);
            
            unset($whereUpcoming['Orders.delivery_date > ']);
            $whereUpcoming = array_merge($whereUpcoming,$new_con);            
            
            $whereCancel = array_merge($whereCancel,$new_con);            
        }
        
        $contain = [
            'Tanks' => ['fields' => ['id','name','name_es','icon'] ]
        ];

        $select = []; //['id','driver_id','tank_id','payment_method','order_type','quantity','total','delivery_date','delivery_address','start_address'];

        $order = ['Orders.delivery_date DESC'];
        $group = ['Orders.id'];

        $info['CurrentOrders'] = $this->Orders->find()
                ->hydrate(false)
                ->where($whereCurrent)                        
                ->order($order)
                ->contain($contain)
                ->select($select)
                ->group($group)
                ->toArray();

        $info['PreviousOrders'] = $this->Orders->find()
                ->hydrate(false)
                ->where($wherePrevious)
                ->contain($contain)
                ->select($select)
                ->group($group)
                ->order($order)
                ->toArray();

        $info['UpcomingOrders'] = $this->Orders->find()
                ->hydrate(false)
                ->where($whereUpcoming)                        
                ->contain($contain)
                ->select($select)
                ->order($order)
                ->group($group)
                ->toArray();

        $info['CancelledOrders'] = $this->Orders->find()
                ->hydrate(false)
                ->where($whereCancel)    
                ->contain($contain)
                ->select($select)
                ->order($order)
                ->group($group)
                ->toArray();   

        $info = $this->updateKeyFields($info);
        
        return $info;
    }  
    
    /* 	
     * function name 	: __getDatesInMonth - all all dates in a given month
     * Author        	: Anu Gupta
     * Date          	: Oct 7, 2017
     */

    function __getDatesInMonth() {
        
        $past_weeks = 4;
        $relative_time = time();
        $weeks = $dates = [];

        for($week_count = 1; $week_count <= $past_weeks; $week_count++) {
            
            $monday = strtotime("last Monday", $relative_time);
            $sunday = strtotime("Sunday", $monday);
            
            $mon = date("Y-m-d", $monday);
            $tue = date("Y-m-d", strtotime($mon.'+1 day'));
            $wed = date("Y-m-d", strtotime($mon.'+2 day'));
            $thu = date("Y-m-d", strtotime($mon.'+3 day'));
            $fri = date("Y-m-d", strtotime($mon.'+4 day'));
            $sat = date("Y-m-d", strtotime($mon.'+5 day'));
            $sun = date("Y-m-d", $sunday);
            
            $weeks[$mon] =  ['dayTotal' => 0.0, 'date' => $mon];
            $weeks[$tue] =  ['dayTotal' => 0.0, 'date' => $tue];
            $weeks[$wed] =  ['dayTotal' => 0.0, 'date' => $wed];
            $weeks[$thu] =  ['dayTotal' => 0.0, 'date' => $thu];
            $weeks[$fri] =  ['dayTotal' => 0.0, 'date' => $fri];
            $weeks[$sat] =  ['dayTotal' => 0.0, 'date' => $sat];
            $weeks[$sun] =  ['dayTotal' => 0.0, 'date' => $sun];
            
            $relative_time = $monday;
            
            if($week_count == 1){
                $dates['end_date'] = $sun;
            }
            if($week_count == $past_weeks){
                $dates['start_date'] = $mon;
            }            
        }
        $resp['date'] = $dates;
        $resp['weeks'] = $weeks;
        return $resp;        
    }

    /* Remove Null values (For Web-Services) */

    public function removeNULL($data=null) {

        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $data [$key] = $this->removeNULL($val);
            }
            return $data;
        } else {
            if ($data === null) {
                $data = "";
            }
            return $data;
        }
    }
    
    /* Remove Null values (For Web-Services) */

    public function makeFloat($val=null) {

        $val = $val > 0 ? (float) number_format($val,2,'.','') : 0.0;
        return $val;        
    }
}
