<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Utility\Hash;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function initialize() {
        parent::initialize();
        $this->loadComponent('Upload');
        $this->loadComponent('SendEmail');
        $this->loadComponent('Paginator');
    }

    public function index() {

        $this->viewBuilder()->layout('admin');
        $this->setUsers();
    }

    /**
     * View add_user
     * @param string|null $id User id.
     * */
    public function add_user($id = null) {
        
        $this->viewBuilder()->layout('admin');

        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->newEntity();

        if (!empty($this->request->data)) {
            //prx($this->request->data);

            //new record
            $flash_msg = __('User has been updated successfully.');

            if (empty($this->request->data['id'])) {

                $this->request->data['created'] = date('Y-m-d H:i:s');
                $this->request->data['modified'] = date('Y-m-d H:i:s');

                $chkUser = $usersTable->find()
                        ->where(['Users.user_type_id' => 1,'Users.is_deleted' => 1, 'Users.email' => $this->request->data['email']])
                        ->select(['Users.id', 'Users.email', 'Users.is_deleted'])
                        ->first();

                if (!empty($chkUser)) {
                    $this->request->data['id'] = $chkUser->id;
                    $this->request->data['is_deleted'] = 0;
                }
                
                //
                $this->request->data['password'] = $usersTable->generate_password();
                
                //dd($this->request->data);								
                $flash_msg = __('User has been added successfully.');
            }
            
            $send_mail = false;            
            if (!empty($this->request->data['password'])) {
                $this->request->data['confirm_password'] = $this->request->data['password'];
                $this->request->data['password'] = md5($this->request->data['password']);//(new DefaultPasswordHasher)->hash($this->request->data['password']);
                
                $send_mail = true;
            }else{
                unset($this->request->data['password']);
            }
            
            if (!empty($this->request->data['picture']['name'])) {

                $imgName = pathinfo($this->request->data['picture']['name']);
                $ext = strtolower($imgName['extension']);

                //echo USER_IMG; die;

                if (in_array($ext, array('jpg', 'jpeg', 'png', 'gif'))) {

                    $destination = USER_IMG;
                    $destThumb = USER_IMG_THUMB;

                    $filename = time() . '-' . $this->request->data['picture']['name'];
                    $file = $this->request->data['picture'];

                    $result_thumb = $this->Upload->upload($file, $destThumb, $filename, array('type' => 'resizecrop', 'size' => array(200, 200), 'quality' => '100'));
                    $result = $this->Upload->upload($file, $destination, $filename, array());

                    if ($result) {
                        if (!empty($this->request->data['old_image'])) {
                            unlink($destination . $this->request->data['old_image']);
                            unlink($destThumb . $this->request->data['old_image']);
                        }
                        $this->request->data['image'] = $filename;
                    }
                }
            }            
           
            $this->request->data['user_type_id'] = 1;
            
            $user = $usersTable->newEntity($this->request->data);            
            //prx($user);

            if ($usersTable->save($user)) {
                
                if($send_mail){
                    $this->sendCredentails($this->request->data);
                }
                
                $this->Flash->success($flash_msg);
                $this->redirect(array('controller' => 'users', 'action' => 'index'));
            }
        } else {

            if (!empty($id)) {
                $id = base64_decode($id);
                $user = $usersTable->find()->where(['id' => $id])->first();
                $user->password = '';
            }
        }

        $this->set('user', $user);
    }
    
    function sendCredentails($data){
        
        if(!empty($data)){
            
            $user_name = $data['email'];
            $pass = $data['confirm_password'];
            $name = $data['first_name'];
            
            $emailTemplateTable = TableRegistry::get('EmailTemplate');
            $emailTemplate = $emailTemplateTable->getEmailTemplate('login-credentials');            
            
            $Useremail['to'] = $data['email'];
            $Useremail['subject'] = $emailTemplate['subject'];            
            
            $Useremail['template'] = str_replace(['{Username}', '{Password}', '{Name}'], [$user_name, $pass, $name], $emailTemplate['description']);                        
            //prx($Useremail);

            $this->SendEmail->sendEmail($Useremail);            
            
        }        
    }

    function setUsers() {

        $resp = array();

        // set paging limit
        $paginate_limit = $this->paginate_limit;
        $paginglimit = $this->_fetchPagingOptions();
        $order = 'DESC';
        $order_by = 'Users.created';
        $page = 1;

        $render = false;
        $this->loadModel('Users');
        
        $where = ['Users.is_deleted' => 0, 'Users.user_type_id' => 1];
        
        if ($this->request->is('ajax') && !empty($this->request->data)) {

            $render = true;
            $data = $this->request->data;
            //prx($data);            
            
            if (!empty($data['page_limt'])) {
                $paginate_limit = $data['page_limt'];
            }
            if (!empty($data['page']) && $data['page'] > 1) {
                $page = $data['page'];
            }
            if (!empty($data['order'])) {
                $order = $data['order'];
            }
            if (!empty($data['order_by'])) {
                $order_by = $data['order_by'];
            }
            
            if(!empty($data['keyword'])){
                $where = array_merge($where,['OR'=>[
                        'Users.first_name LIKE'=>'%'.$data['keyword'].'%', 'Users.last_name LIKE'=>'%'.$data['keyword'].'%',
                        'Users.email' => $data['keyword']
                    ]
                ]);
            }
            
            if(!empty($data['status'])){
                $where = array_merge($where,['Users.status'=>$data['status']]);
            }
            
            #delete
            if (!empty($data['actions']) && $data['actions'] == 1) {
                $resp['actions'] = 'delete';
                if (!empty($data['user_id'])) {
                    $this->Users->updateAll(['is_deleted' => 1], ['id IN' => $data['user_id']]);
                    if ($page > 1) {
                        $count = $this->Users->find()
                                ->where($where)
                                ->limit($paginate_limit)
                                ->page($page)
                                ->all()
                                ->count();
                        if ($count == 0) {
                            $page = $page - 1;
                        }
                    }
                }
            }
        }
        
        if ($page > 1) {
            $count = $this->Users->find()
                    ->where($where)
                    ->limit($paginate_limit)
                    ->page($page)
                    ->all()
                    ->count();
            if ($count == 0) {
                $page = 1;
            }
        }
        
        $this->paginate = [
            'sortWhitelist' => ['Users.id','Users.first_name','Users.last_name','Users.email','Users.created','Users.status'],
            'conditions' => $where,
            'limit' => $paginate_limit,
            'page' => $page,
            'order' => [$order_by => $order]
        ];
        $users = $this->paginate('Users')->toArray();
        //prx($users);

        $this->set(compact('users', 'paginglimit', 'paginate_limit', 'order', 'order_by'));

        if ($render) {
            $this->viewBuilder()->layout(false);
            $resp['status'] = 1;
            $resp['html'] = $this->render('/Element/Admin/user_filter')->body();
            echo json_encode($resp);
            die;
        }
    }

    public function vendors() {

        $this->viewBuilder()->layout('admin');
        $this->setVendors();
    }

    function setVendors() {

        $resp = array();

        // set paging limit
        $paginate_limit = $this->paginate_limit;
        $paginglimit = $this->_fetchPagingOptions();
        $order = 'DESC';
        $order_by = 'Users.id';
        $page = 1;

        $render = false;
        $this->loadModel('Users');
        
        $where = ['Users.is_deleted' => 0, 'Users.user_type_id' => 2];

        if ($this->request->is('ajax') && !empty($this->request->data)) {

            $render = true;
            $data = $this->request->data;
            //prx($data);
            
            if (!empty($data['page_limt'])) {
                $paginate_limit = $data['page_limt'];
            }
            if (!empty($data['page']) && $data['page'] > 1) {
                $page = $data['page'];
            }
            if (!empty($data['order'])) {
                $order = $data['order'];
            }
            if (!empty($data['order_by'])) {
                $order_by = $data['order_by'];
            }
            
            if(!empty($data['keyword'])){
                $where = array_merge($where,['OR'=>[
                        'Users.first_name LIKE'=>'%'.$data['keyword'].'%', 'Users.last_name LIKE'=>'%'.$data['keyword'].'%',
                        'Users.email' => $data['keyword']
                    ]
                ]);
            }
            
            if(!empty($data['status'])){
                $where = array_merge($where,['Users.status'=>$data['status']]);
            }
            
            #delete
            if (!empty($data['actions']) && $data['actions'] == 1) {
                $resp['actions'] = 'delete';
                if (!empty($data['user_id'])) {
                    $this->Users->updateAll(['is_deleted' => 1], ['id IN' => $data['user_id']]);
                    if ($page > 1) {
                        $count = $this->Users->find()
                                ->where($where)
                                ->limit($paginate_limit)
                                ->page($page)
                                ->all()
                                ->count();
                        if ($count == 0) {
                            $page = $page - 1;
                        }
                    }
                }
            }
        }
        
        if ($page > 1) {
            $count = $this->Users->find()
                    ->where($where)
                    ->limit($paginate_limit)
                    ->page($page)
                    ->all()
                    ->count();
            if ($count == 0) {
                $page = 1;
            }
        }
        
        $this->paginate = [
            'sortWhitelist' => ['Users.id','Users.first_name','Users.last_name','Users.email','Users.driver_total','Users.created','Users.status'],
            'conditions' => $where,
            'limit' => $paginate_limit,
            'page' => $page,
            'order' => [$order_by => $order],
            'fields'=> [
                'id', 'user_type_id','first_name','last_name','email','image','status','created','online_status','is_login',
                'driver_total' => '(select sum(total) from orders where Users.id = orders.driver_id and orders.status = 3)'
            ]
        ];
        
        
        $vendors = $this->paginate('Users')->toArray();
        //prx($vendors);

        $this->set(compact('vendors', 'paginglimit', 'paginate_limit', 'order', 'order_by'));

        if ($render) {
            $this->viewBuilder()->layout(false);

            $resp['status'] = 1;
            $resp['html'] = $this->render('/Element/Admin/vendor_filter')->body();
            echo json_encode($resp);
            die;
        }
    }

    /**
     * View add_vendor
     * @param string|null $id Vendor id.
     * */
    public function add_vendor($id = null) {

        $this->viewBuilder()->layout('admin');
        
        $UsersTable = TableRegistry::get('Users');
        $users = $UsersTable->newEntity();

        if (!empty($this->request->data)) {

            //prx($this->request->data);
            //new record
            $flash_msg = __('Driver has been updated successfully.');

            if (empty($this->request->data['id'])) {

                $this->request->data['created'] = date('Y-m-d H:i:s');
                $this->request->data['modified'] = date('Y-m-d H:i:s');

                $chkVendor = $UsersTable->find()
                        ->where(['Users.is_deleted' => 1, 'Users.email' => $this->request->data['email'], 'Users.user_type_id' => 2])
                        ->select(['Users.id', 'Users.email', 'Users.is_deleted'])
                        ->first();

                if (!empty($chkVendor)) {
                    $this->request->data['id'] = $chkVendor->id;                    
                }

                $this->request->data['password'] = $UsersTable->generate_password();
                
                //dd($this->request->data);								
                $flash_msg = __('Driver has been added successfully.');
                
                $this->request->data['password']['created'] = DATABASE_FORMAT_CURRENT_DATE_TIME;
            }            

            if (!empty($this->request->data['picture']['name'])) {

                $imgName = pathinfo($this->request->data['picture']['name']);
                $ext = strtolower($imgName['extension']);                

                if (in_array($ext, array('jpg', 'jpeg', 'png', 'gif'))) {

                    $destination = USER_IMG;
                    $destThumb = USER_IMG_THUMB;

                    $filename = time() . '-' . $this->request->data['picture']['name'];
                    $file = $this->request->data['picture'];

                    $result_thumb = $this->Upload->upload($file, $destThumb, $filename, array('type' => 'resizecrop', 'size' => array(200, 200), 'quality' => '100'));
                    //prx($this->Upload->errors);
                    
                    $result = $this->Upload->upload($file, $destination, $filename, array());

                    if ($result) {
                        if (!empty($this->request->data['old_image'])) {
                            unlink(USER_IMG . $this->request->data['old_image']);
                            unlink(USER_IMG_THUMB . $this->request->data['old_image']);
                        }
                        $this->request->data['image'] = $filename;
                    }
                }
            }            
           
            $this->request->data['user_type_id'] = 2;            
            $this->request->data['is_deleted'] = 0;            
            
            $send_mail = false;
            if (!empty($this->request->data['password'])) {
                $this->request->data['confirm_password'] = $this->request->data['password'];
                $this->request->data['password'] = md5($this->request->data['password']); //new DefaultPasswordHasher)->hash($this->request->data['password']);
                $send_mail = true;
            }else{
                unset($this->request->data['password']);
            }            

            $Users = $UsersTable->newEntity($this->request->data);            
            
            if ($UsersTable->save($Users)) {                
                
                if($send_mail){
                    $this->sendCredentails($this->request->data);
                }
                
                $this->Flash->success($flash_msg);
                $this->redirect(array('controller' => 'users', 'action' => 'vendors'));
            }
        }

        if (!empty($id)) {
            $id = base64_decode($id);
            $users = $UsersTable->find()->where(['id' => $id])->first();
            $users->password = '';
        }

        $this->set('users', $users);
    }
    
    function change_password($id = null){
        
        $this->loadModel('Users');
        $this->viewBuilder()->layout(false);
        
        if (!empty($id)) {
            $id = base64_decode($id);
            $user = $this->Users->find()
                                ->where(['id' => $id])
                                ->select(['id','first_name','email'])
                                ->first();
        }
        
        if (!empty($this->request->data)) {            
            
            //prx($this->request->data);
            $data = $this->request->data;
            $this->request->data['password'] = md5($this->request->data['password']);            
            
            $this->request->data = $this->Users->patchEntity($user,$this->request->data);
            $this->Users->save($this->request->data);
            
            $data['email'] = $user->email;
            $data['first_name'] = $user->first_name;
                        
            $this->sendCredentails($data);
            
            $this->Flash->success(__('Password has been updated successfully.'));
            $this->redirect($this->referer());
        }        
        
        $this->set(compact('id','user'));
    }
    
    function check_user_email($user_id = 0) {

        $email = trim($_GET['email']);
        $table = TableRegistry::get('Users');

        $count = $table->find()
                ->where(['Users.user_type_id' => 1,'Users.id !=' => $user_id, 'Users.email' => $email, 'Users.is_deleted' => 0])
                ->count();

        if ($count > 0) {
            echo 'false';
        } else {
            echo 'true';
        }
        exit;
    }
    
    function check_driver_email($user_id = 0) {

        $email = trim($_GET['email']);
        $table = TableRegistry::get('Users');

        $count = $table->find()
                ->where(['Users.user_type_id' => 2,'Users.id !=' => $user_id, 'Users.email' => $email, 'Users.is_deleted' => 0])
                ->count();

        if ($count > 0) {
            echo 'false';
        } else {
            echo 'true';
        }
        exit;
    }
    
    function driver_filling($user_id=null){
        
        $this->viewBuilder()->layout(false);
        $filling = '';
        
        $this->loadModel('DriverFillings');
        $user_id = base64_decode($user_id);
        
        if(!empty($this->request->data)){            
            
            //prx($this->request->data);            
            $this->DriverFillings->deleteAll(['user_id' => $user_id]);
            
            $data = $this->DriverFillings->newEntities($this->request->data['data']);            
            $this->DriverFillings->saveMany($data);            
            
            $this->Flash->success(__('Tank details has been updated.'));
            $this->redirect(array('controller' => 'users', 'action' => 'vendors'));
        }

        if(!empty($user_id)){
            
            $this->loadModel('Tanks');
            $tanks = $this->Tanks->find()
                                    ->hydrate(false)
                                    ->where(['status' => 1, 'is_deleted' => 0])
                                    ->select(['id','name','name_es'])
                                    ->order(['category_id' => 'ASC', 'sequence' => 'ASC'])
                                    ->toArray();
            //prx($tanks);            
                    
            $filling = $this->DriverFillings->find()
                                                ->hydrate(false)
                                                ->select(['id','user_id','tank_id','quantity'])
                                                ->where(['user_id' => $user_id])                                                
                                                ->toArray();
            //prx($filling);
        }
        
        $this->set(compact('user_id','filling','tanks'));        
    }
    
    function tank_price($user_id=null){
        
        $this->viewBuilder()->layout('admin');        
        
        $this->loadModel('TankPrices');        
        
        if(!empty($this->request->data)){
            
            $this->request->data['id'] = 1;
            $data = $this->TankPrices->newEntity($this->request->data);
            
            $this->TankPrices->save($data);
            
            $this->Flash->success(__('Tank price has been updated.'));
            $this->redirect($this->referer());
        }            
        
        $info = $this->TankPrices->find()->first();
        $this->set(compact('info'));        
    }
    
    function taxes(){
        
        $this->viewBuilder()->layout('admin');        
        
        $this->loadModel('Taxes');        
        
        if(!empty($this->request->data)){
            
            $this->request->data['id'] = 1;
            $data = $this->Taxes->newEntity($this->request->data);
            
            $this->Taxes->save($data);
            
            $this->Flash->success(__('Tax details has been updated.'));
            $this->redirect($this->referer());
        }            
        
        $info = $this->Taxes->find()->first();
        $this->set(compact('info'));        
    }
    function add_tank($id = null){
        
        $this->viewBuilder()->layout('admin');

        $tanksTable = TableRegistry::get('Tanks');
        $categoriesTable = TableRegistry::get('Categories');
        
        $t = $categoriesTable ->find();
        $categories = $t->select([
                            'id',
                            'name','name_es'
                            ])->hydrate(false)->toArray();
        
       $categories = Hash::combine($categories, '{n}.id', ['%s / %s', '{n}.name', '{n}.name_es']);
       
        $tank = $tanksTable->newEntity();

        if (!empty($this->request->data)) {
            //prx($this->request->data);

            //new record
            $flash_msg = __('Tank has been updated successfully.');

            if (empty($this->request->data['id'])) {

                $this->request->data['created'] = DATABASE_FORMAT_CURRENT_DATE_TIME;
                
                //dd($this->request->data);								
                $flash_msg = __('Tank has been added successfully.');
            }
            
            
            if (!empty($this->request->data['icon']['name'])) {

                $imgName = pathinfo($this->request->data['icon']['name']);
                $ext = strtolower($imgName['extension']);

                if (in_array($ext, array('jpg', 'jpeg', 'png', 'gif'))) {

                    $destination = TANK_IMG;
                    $destThumb = TANK_IMG_THUMB;

                    $filename = time() . '-' . $this->request->data['icon']['name'];
                    $file = $this->request->data['icon'];

                    //$result_thumb = $this->Upload->upload($file, $destThumb, $filename, array('type' => 'resizecrop', 'size' => array(100, 100), 'quality' => '100'));
                    //prx($this->Upload->errors);
                    $result = $this->Upload->upload($file, $destination, $filename, array());

                    if ($result) {
                        if (!empty($this->request->data['old_image'])) {
                            unlink($destination . $this->request->data['old_image']);
                            unlink($destThumb . $this->request->data['old_image']);
                        }
                        $this->request->data['icon'] = $filename;
                    }
                }
            } else {
                unset($this->request->data['icon']);
            }            
           
           
            $tank = $tanksTable->newEntity($this->request->data);         
            if ($tanksTable->save($tank)) {
               
                $this->Flash->success($flash_msg);
                $this->redirect(array('controller' => 'users', 'action' => 'list_tanks'));
            }
        } else {

            if (!empty($id)) {
                $id = base64_decode($id);
                $tank = $tanksTable->find()->where(['id' => $id])->first();
                
            }
        }

        $this->set(compact('tank', 'categories'));
    }
    public function list_tanks() {

        $this->viewBuilder()->layout('admin');
        $this->setTanks();
    }

    function setTanks() {

        $resp = array();

        // set paging limit
        $paginate_limit = $this->paginate_limit;
        $paginglimit = $this->_fetchPagingOptions();
        $order = 'DESC';
        $order_by = 'Tanks.id';
        $page = 1;

        $render = false;
        $this->loadModel('Tanks');
        
        $where = ['Tanks.is_deleted' => 0];

        if ($this->request->is('ajax') && !empty($this->request->data)) {

            $render = true;
            $data = $this->request->data;
            //prx($data);

            if (!empty($data['page_limt'])) {
                $paginate_limit = $data['page_limt'];
            }
            if (!empty($data['page']) && $data['page'] > 1) {
                $page = $data['page'];
            }
            if (!empty($data['order'])) {
                $order = $data['order'];
            }
            if (!empty($data['order_by'])) {
                $order_by = $data['order_by'];
            }
            
            if(!empty($data['keyword'])){
                $where = array_merge($where,['OR'=>[
                        'Tanks.name LIKE'=>'%'.$data['keyword'].'%',
                        'Tanks.price' => $data['keyword']
                    ]
                ]);
            }
            
            if(!empty($data['status'])){
                $where = array_merge($where,['Tanks.status'=>$data['status']]);
            }
            
            if(!empty($data['category_id'])){
                $where = array_merge($where,['Tanks.category_id'=>$data['category_id']]);
            }
            
            #delete
            if (!empty($data['actions']) && $data['actions'] == 1) {
                $resp['actions'] = 'delete';
                if (!empty($data['tank_id'])) {
                    $this->Tanks->updateAll(['is_deleted' => 1], ['id IN' => $data['tank_id']]);
                    if ($page > 1) {
                        $count = $this->Tanks->find()
                                ->where($where)
                                ->limit($paginate_limit)
                                ->page($page)
                                ->all()
                                ->count();
                        if ($count == 0) {
                            $page = $page - 1;
                        }
                    }
                }
            }
        }
        
        if ($page > 1) {
            $count = $this->Tanks->find()
                    ->where($where)
                    ->limit($paginate_limit)
                    ->page($page)
                    ->all()
                    ->count();
            if ($count == 0) {
                $page = 1;
            }
        }
        
        $this->paginate = [
            //'sortWhitelist' => ['Tanks.id','Tanks.name','Tanks.name_es','Tanks.price','Tanks.created','Tanks.category_id'],
            'conditions' => $where,
            'limit' => $paginate_limit,
            'page' => $page,
            'contain' => ['Categories' => ['fields' => ['id','name','name_es']]],
            'order' => [$order_by => $order]            
        ];
        
        
        $tanks = $this->paginate('Tanks')->toArray();
        //prx($tanks);

        $this->set(compact('tanks', 'paginglimit', 'paginate_limit', 'order', 'order_by'));

        if ($render) {
            $this->viewBuilder()->layout(false);

            $resp['status'] = 1;
            $resp['html'] = $this->render('/Element/Admin/tank_filter')->body();
            echo json_encode($resp);
            die;
        }
    }
}
