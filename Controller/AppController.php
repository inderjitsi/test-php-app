<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Routing\Router;
use Cake\Controller\Component\AuthComponent;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use Cake\Auth\DefaultPasswordHasher;

ob_start();

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    //If you want to update some fields, like the last_login_date, or last_login_ip, just do :

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    var $paginate_limit = 10;
    public $myGlobalSession;    

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event) {

        /*if (!array_key_exists('_serialize', $this->viewVars) &&
                in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            //$this->set('_serialize', true);
        }*/
    }

    public function beforeFilter(Event $event) {

    }         
    
    function checkOldPassword(){
        
        $user_id = $this->Auth->user('id');
        $pass = (new DefaultPasswordHasher)->hash($this->request->data['old_password']);

        $this->loadModel('Users');
        $info = $this->Users->find()
                                ->hydrate(false)
                                ->where(['Users.id' => $user_id])
                                ->select(['id','password'])
                                ->first();            

        $currentPassword = $info['password'];
        $check_pass = (new DefaultPasswordHasher)->check($this->request->data['old_password'], $currentPassword);
        return $check_pass;
    }    
}
