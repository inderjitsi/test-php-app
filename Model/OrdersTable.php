<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\HasMany $Comments
 * @property \Cake\ORM\Association\HasMany $Reports
 * @property \Cake\ORM\Association\HasMany $ReviewRatings
 * @property \Cake\ORM\Association\HasMany $TaskOffers
 * @property \Cake\ORM\Association\HasMany $Tasks
 * @property \Cake\ORM\Association\HasMany $Transactions
 * @property \Cake\ORM\Association\HasMany $UserDetails
 * @property \Cake\ORM\Association\HasMany $UserDevices
 * @property \Cake\ORM\Association\HasMany $UserImages
 * @property \Cake\ORM\Association\HasMany $UserSkills
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {

        $this->belongsTo(
            'Users', [
                'foreignKey' => 'user_id'
            ]    
        );
        
        $this->belongsTo(
            'Driver', [
                'className' => 'Users',
                'foreignKey' => 'driver_id'
            ]    
        );
        
        $this->belongsTo(
            'Tanks', [
                'foreignKey' => 'tank_id'
            ]    
        );
        
        $this->hasMany(
            'OrderLogs', [
                'foreignKey' => 'order_id',
                'dependent' => true
            ]
        );
        
        $this->hasMany(
            'Notifications', [
                'foreignKey' => 'order_id',
                'dependent' => true
            ]
        );
        
        $this->hasOne(
            'Ratings', [
                'foreignKey' => 'order_id',
                'dependent' => true
            ]    
        );
        
        $this->hasOne(
            'OrderPayments', [
                'foreignKey' => 'order_id',
                'dependent' => true
            ]    
        );
    }
    
    function saveOrder($order = null, $driver = null) {

        //$amount = $this->getAmount($order['size']);        
        $priceInfo = $this->calcTotal($order);
        #prx($priceInfo);
        
        $order = $order + $priceInfo;        

        if (empty($order['id'])) {
            $order['created'] = DATABASE_FORMAT_CURRENT_DATE_TIME;
        }        
        #$order['id'] = 2; 

        $order = $this->newEntity($order);        
        //prx($order);
        
        $this->save($order);
        $order_id = $order->id;
        
        $order_types    = Configure::read('ORDER_TYPES');        
        #Log::write('error', "Checkout Driver: " . $driver['id']);
        
        $tank = $this->getTankData($order['tank_id']);
        
        //notification to driver
        if(!empty($driver)){
            $newData['order_id']    = $order_id;
            $newData['device']      = $driver['device'];
            $newData['device_token']= $driver['device_token'];
            $newData['user_id']     = $order->user_id;
            $newData['driver_id']   = $driver['id'];
            $newData['type']        = 'order';
            $newData['message']     = __('You have received '.$order->order_type.' '.$order_types[$order->order_type].' order of').' '.$tank['name'];
            $newData['to']          = 'driver';        

            $newData['clickAction'] = 'notify_action';
            $newData['title']       = $newData['message']; 
            $newData['icon']        = 'ic_push';

            $newData['delivery_date']= $order->delivery_date;
            $newData['delivery_address']  = $order->delivery_address;
            //prx($newData);

            $this->send_notificatin($newData);
        }

        return $order_id;
    }
    
    function calcTotal($data = null){
        
        $fare = $this->getFareDetails();        
        $tank = $this->getTankData($data['tank_id']);
        //prx($tank);
        
        $price = ($tank['price'] * $data['quantity']);
        $tax = ($price * $fare['tax']) / 100;
        
        $total = $price + $tax + $fare['delivery_fee'];
        
        $resp = [];
        $resp['tank_price']     = $tank['price'];
        $resp['quantity']       = $data['quantity'];
        $resp['item_total']     = $price;
        $resp['delivery_fee']   = $fare['delivery_fee'];        
        $resp['tax_percent']    = $fare['tax'];
        $resp['tax']            = $tax;
        $resp['total']          = $total;
        
        $resp = $this->numberFormat($resp);
        return $resp;
    }
            
    function getTankData($tank_id = null) {

        $table = TableRegistry::get('Tanks');

        $info = $table->find()
                ->hydrate(false)
                ->where(['id' => $tank_id])
                ->select(['id','name','name_es','price'])
                ->first();
        //prx($info);
        
        return $info; 
    }
    
    function getFareDetails(){
        
        $table = TableRegistry::get('Taxes');

        $info = $table->find()
                ->hydrate(false)
                ->first();        
        
        return $info;
    }
    
    /* 	
     * function name 	: accpetOrderCase
     * Author        	: Inderjit Singh
     * Date          	: Sep 26, 2017
     * params 		: order_id
     */

    function accpetOrderCase($order_id = null, $driver_id = null) {

        if (!empty($order_id)) {
            
            $order = $this->find()
                    ->where(['Orders.id' => $order_id])
                    ->contain(['Users' => ['fields' => ['id', 'user_type_id', 'email', 'device', 'device_token']]])
                    ->first();
            //prx($order);

            if (!empty($order)) {

                //update driver lat long in orders table
                if (!empty($driver_id)) {
                    
                    $userTable = TableRegistry::get('Users');
                    
                    $driver = $this->Users->find()->hydrate(false)
                            ->where(['id' => $driver_id])
                            ->select(['id', 'latitude', 'longitude'])
                            ->first();
                    //prx($driver);                    

                    if (!empty($driver)) {

                        $order->dr_latitude = $driver['latitude'];
                        $order->dr_longitude = $driver['longitude'];

                        $start_address = $this->getAddressFromLatLong($driver['latitude'], $driver['longitude']);
                        $order->start_address = $start_address;

                        $this->save($order);
                    }
                }

                // send notification to customer
                $newData['order_id'] = $order_id;
                $newData['device'] = $order['user']->device;
                $newData['device_token'] = $order['user']->device_token;
                $newData['user_id'] = $order['user']->id;
                $newData['type'] = 'order_accepted';
                $newData['message'] = __('Your order has been accepted by driver.');
                $newData['to'] = 'user';
                //prx($newData);

                $this->send_notificatin($newData);
            }
        }
    }
    
    /* 	
     * function name 	: outDeliveryOrderCase
     * Author        	: Inderjit Singh
     * Date          	: Sep 28, 2017
     * params 		: order_id
     */

    function outDeliveryOrderCase($order_id = null) {

        if (!empty($order_id)) {
            
            $order = $this->find()
                    ->hydrate(false)
                    ->where(['Orders.id' => $order_id])
                    ->contain(['Users' => ['fields' => ['id', 'user_type_id', 'email', 'device', 'device_token']]])
                    ->first();
            //prx($order);

            if (!empty($order)) {

                // send notification to customer
                $newData['order_id'] = $order_id;
                $newData['device'] = $order['user']['device'];
                $newData['device_token'] = $order['user']['device_token'];
                $newData['user_id'] = $order['user']['id'];
                $newData['type'] = 'out_for_delivery';
                $newData['message'] = __('Your order has out for delivery.');
                $newData['to'] = 'user';

                $this->send_notificatin($newData);
            }
        }
    }
    
    /* 	
     * function name 	: completeOrderCase
     * Author        	: Inderjit Singh
     * Date          	: Sep 28, 2017
     * params 		: order_id
     */

    function completeOrderCase($order_id = null) {

        $resp['status'] = 0;
        $resp['message'] = $message = '';

        if (!empty($order_id)) {

            $order = $this->find()
                    ->hydrate(false)
                    ->where(['Orders.id' => $order_id])
                    ->contain([
                        'OrderPayments',
                        'Users' => ['fields' => ['id', 'user_type_id', 'first_name', 'last_name', 'email','device','device_token']],
                        'Driver' => [
                            'fields' => ['id', 'user_type_id', 'email'],
                            'DriverFillings' => [
                                'Tanks' => ['fields' => ['id', 'name', 'name_es'] ] 
                            ]
                        ]
                    ])
                    ->first();
            //prx($order);

            if ($order['payment_method'] == 'cc' && !empty($order['card_id']) && empty($order['order_payment'])) {

                $this->CustomerCreditCards = TableRegistry::get('CustomerCreditCards');
                $cardInfo = $this->CustomerCreditCards->find()
                        ->hydrate(false)
                        ->where(['CustomerCreditCards.id' => $order['card_id']])
                        ->contain(['Users' => ['fields' => ['id', 'stripe_customer_id']]])
                        ->first();
                //prx($cardInfo);

                if (!empty($cardInfo)) {

                    $this->setStripeInit();

                    try {
                        // Charge the user's card:
                        $create_charge = \Stripe\Charge::create(array(
                                    "amount" => $order['total'] * 100,
                                    "currency" => "usd",
                                    "metadata" => array("order_id" => $order['id']),
                                    "customer" => $cardInfo['user']['stripe_customer_id'],
                        ));

                        $payInfo = $create_charge->__toArray(true);
                        //prx($create_charge);

                        if ($payInfo && !isset($payInfo['error'])) {

                            $this->OrderPayments = TableRegistry::get('OrderPayments');

                            $payData['order_id'] = $order['id'];
                            $payData['user_id'] = $order['user_id'];
                            $payData['transaction_id'] = $payInfo['id'];
                            $payData['amount'] = $payInfo['amount'];
                            $payData['failure_code'] = $payInfo['failure_code'];
                            $payData['failure_message'] = $payInfo['failure_message'];

                            $payData['transaction_fee'] = ( ( ($order['total'] * 2.9) / 100 ) + 0.30 );
                            $payData['created'] = DATABASE_FORMAT_CURRENT_DATE_TIME;

                            $payData = $this->OrderPayments->newEntity($payData);

                            $this->OrderPayments->save($payData);

                            $oData['id'] = $order['id'];
                            $oData['payment_id'] = $payData->id;

                            $oData = $this->newEntity($oData);
                            $this->save($oData);
                        } else {
                            $message = $payInfo['error'];
                        }
                    } catch (\Stripe\Error\ApiConnection $e) {
                        // Network problem, perhaps try again.
                        $e_json = $e->getJsonBody();
                        $message = $e_json['error']['message'];
                    } catch (\Stripe\Error\InvalidRequest $e) {
                        // You screwed up in your programming. Shouldn't happen!
                        $e_json = $e->getJsonBody();
                        $message = $e_json['error']['message'];
                    } catch (\Stripe\Error\Api $e) {

                        $e_json = $e->getJsonBody();
                        $message = $e_json['error']['message'];
                    } catch (\Stripe\Error\Card $e) {

                        $e_json = $e->getJsonBody();
                        $message = $e_json['error']['message'];
                    }
                } else {
                    $message = __('Customer credit card not found.');
                }
            }

            if (empty($message)) {

                $resp['status'] = 1;

                if (!empty($order['driver']['driver_filling'])) {

                    $DriverFillings = TableRegistry::get('DriverFillings');

                    //update driver qty
                    $driver_filling = $order['driver']['driver_filling'];
                    $driver_filling['quantity'] = $driver_filling['quantity'] - $order['quantity'];

                    $driver_filling = $DriverFillings->newEntity($driver_filling);
                    $DriverFillings->save($driver_filling);
                }

                //send an email (order details) to customer
                $locale = $this->getLocal();

                $EmailTemplate = TableRegistry::get('EmailTemplate');                    
                $emailTemplate = $EmailTemplate->getEmailTemplate('order_complete');

                $subject = $emailTemplate['subject'];
                $description = $emailTemplate['description'];
                $tank_name = $order['driver']['driver_filling']['tank']['name'];

                if ($locale == 'es') {
                    $subject = $emailTemplate['subject_es'];
                    $description = $emailTemplate['description_es'];
                    $tank_name = $order['driver']['driver_filling']['tank']['name_es'];
                }

                $emailData['to'] = $order['user']['email'];
                $emailData['subject'] = $subject;

                $order_types = Configure::read('ORDER_TYPES');
                $payments = Configure::read('PAYMENT_METHODS');

                $otype = __($order_types[$order['order_type']]);
                $mode = __($payments[$order['payment_method']]);                    

                $price      = '$' . number_format($order['tank_price'], 2);
                $tax        = '$' . number_format($order['tax'], 2);
                $delivery   = '$' . number_format($order['delivery_fee'], 2);
                $total      = '$' . number_format($order['total'], 2);

                $name = $order['user']['first_name'];

                $emailData['template'] = str_replace(
                        ['{Name}', '{OrderID}', '{OrderType}', '{Tank}', '{PaymentMethod}', '{Price}', '{Quantity}', '{Tax}', '{Delivery}', '{Total}'], [$name, $order['id'], $otype, $tank_name, $mode, $price, $order['quantity'], $tax, $delivery, $total ], $description
                );

                $this->sendEmail($emailData);                    
                
                // send notification to customer
                $newData['order_id']        = $order['id'];
                $newData['device']          = $order['user']['device'];
                $newData['device_token']    = $order['user']['device_token'];
                $newData['user_id']         = $order['user_id'];
                $newData['type']            = 'completed';
                $newData['message']         = __('Your order has been completed successfully.');
                $newData['to']              = 'user';
                //prx($newData);

                $this->send_notificatin($newData);
            }
        }

        $resp['message'] = $message;
        return $resp;
    }
    
    function setStripeInit() {

        require_once(ROOT . '/vendor/stripe/stripe-php/init.php');
        \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
    }
    
    public function sendEmail($emailTemplate = array()){
       
       
        $fromName = Configure::read('EMAIL_SETTINGS.FROM_NAME');
        $fromEmail = Configure::read('EMAIL_SETTINGS.FROM_EMAIL');        
        
        try{            
            $email = new Email();            
            $email
              ->template('default')
              ->emailFormat('html')
              ->subject($emailTemplate['subject'])
              ->to($emailTemplate['to'])
              ->bcc(TESTING_MAIL)
              ->from([$fromEmail => $fromName])
              ->viewVars([
                'content'=>$emailTemplate['template'],
                'myProfileLink'=>'',//$emailTemplate['myProfileLink'],
              ]);
              $email->send();
              return 1;
        } catch (\Cake\Network\Exception\SocketException $e){
            
            $message = $e->getMessage();
            return $message;
            
        } catch (Exception $e) {
            
            $message = $e->getMessage();
            return $message;
        }         
    }
    
    /* 	
     * function name 	: getLocal
     * Author        	: Inderjit Singh
     * Date          	: Sep 28, 2017     
     */

    public function getLocal() {

        $locale = 'en';

        $headers = getallheaders();

        if (!empty($headers['locale']) && $headers['locale'] == 'es') {
            $locale = $headers['locale'];
        }
        return $locale;
    }
    
    /* 	
     * function name 	: getAddressFromLatLong
     * Author        	: Inderjit Singh
     * Date          	: Oct 11, 2017
     * params 		: lat, long
     */

    function getAddressFromLatLong($lat = null, $long = null) {

        $address = '';

        if ($lat && $long) {

            $gKey = GOOGLE_API_KEY;
            $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&key=$gKey";
            
            $data = $this->curl_get($url);
            
            if(!empty($data)){
                
                if(!empty($data['results'][0]['formatted_address'])){
                    $address = $data['results'][0]['formatted_address'];
                }                
            }            
        }
        return $address;
    }
    
    function curl_get($url, array $get = NULL, array $options = array()) {

        $defaults = array(
            CURLOPT_URL => $url,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 4
        );

        $ch = curl_init();
        curl_setopt_array($ch, ($options + $defaults));
        if (!$result = curl_exec($ch)) {
            $err = curl_error($ch);
            //CakeLog::write('curl_fail', 'Date ' . date('Y-m-d H:i:s') . ' url: ' . $url . ' -- error --' . $err);
            trigger_error($err);
        }
        curl_close($ch);
        
        if(!empty($result)){
            $result = json_decode($result, true);
        }        
        return $result;
    }
    
    public function numberFormat($data = null, $t_sep = '') {

        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $data [$key] = $this->numberFormat($val);
            }
            return $data;
        } else {
            $data = number_format($data,2,'.',$t_sep);
            return $data;
        }
    }
    
    function send_notificatin($data = null) {

        if (!empty($data)) {
            
            $table = TableRegistry::get('Notifications');
            $notify = $data;
            $notify['created'] = DATABASE_FORMAT_CURRENT_DATE_TIME;                        
            
            $notify = $table->newEntity($notify);
            $table->save($notify);
            
            if ($data['device'] == 'android') {

                return $this->androidNotification($data);
            } else if ($data['device'] == 'ios') {

                return $this->iosNotification($data);
            } else {
                
            }
        }
    }
    
    public function androidNotification($data = null) {
        
        if ($data['to'] == 'user') { // for user
            $apiKey = FCM_CUSTOMER_KEY;
        } else { // for driver
            $apiKey = FCM_DRIVER_KEY;
        }
        
        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = array(
            'to' => $data['device_token'],
            "notification" => [
                "body"        => $data['message'],
                "clickAction" => @$data['clickAction'],
                "icon"        => @$data['icon'],
                "resp"        => $data
            ],
            "data" => $data /*array(
                "type" => $data['type'],
                "order_id" => $data['order_id'],
                "user_id" => $data['user_id'],
                "driver_id" => $data['driver_id']                
            )*/
        );        
        
        $fields = json_encode($fields);        
        
        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);        
        
        $result = curl_exec($ch);
        curl_close($ch);
        
        //prx($result);
        return $result;
    }

    function iosNotification($data = null) {

        // Put your private key's passphrase here:
        $passphrase = IOS_PUSH_PASSPHRASE;
        $ctx = stream_context_create();

        if ($data['to'] == 'user') { // for user
            $pemFile = IOS_CUSTOMER_PEM_FILE;
        } else { // for driver
            $pemFile = IOS_DRIVER_PEM_FILE;
        }

        stream_context_set_option($ctx, 'ssl', 'local_cert', $pemFile);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server	
        $fp = stream_socket_client(IOS_GATEWAY, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if ($fp) {

            // Create the payload body
            $body['aps'] = array(
                'alert' => $data['message'],
                'sound' => 'default',
                'badge' => 1
            );

            $body['data'] = $data;
            
            #Log::write('error', "iOS Notification: " . print_r($body,true));
            
            // Encode the payload as JSON
            $payload = json_encode($body);

            // Build the binary notification
            $msg = chr(0) . pack('n', 32) . pack('H*', $data['device_token']) . pack('n', strlen($payload)) . $payload;

            // Send it to the server
            $result = fwrite($fp, $msg, strlen($msg));

            fclose($fp);
            return true;
        }
    }
}
