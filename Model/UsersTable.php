<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;


/**
 * Users Model
 *
 * @property \Cake\ORM\Association\HasMany $Comments
 * @property \Cake\ORM\Association\HasMany $Reports
 * @property \Cake\ORM\Association\HasMany $ReviewRatings
 * @property \Cake\ORM\Association\HasMany $TaskOffers
 * @property \Cake\ORM\Association\HasMany $Tasks
 * @property \Cake\ORM\Association\HasMany $Transactions
 * @property \Cake\ORM\Association\HasMany $UserDetails
 * @property \Cake\ORM\Association\HasMany $UserDevices
 * @property \Cake\ORM\Association\HasMany $UserImages
 * @property \Cake\ORM\Association\HasMany $UserSkills
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {

        $this->hasOne(
            'DriverFillings', [
                'foreignKey' => 'user_id'
            ]    
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        
         $validator
             ->requirePresence('first_name', 'create')             
             ->notEmpty('first_name',__('Please enter first name.'));
         
        /* $validator
             ->requirePresence('last_name', 'create')
             ->notEmpty('last_name',__('Please enter last name.')); */
        
        $validator            
            ->requirePresence('email', 'create')
            ->notEmpty('email',__('Please enter email.'))
            ->add('email','valid-check',[
               'rule' => 'email',
                'message' => __('Please enter valid email.')
            ])    
            ->add('email', 'unique-check',[
                    'rule' => 'isEmailExist',
                    'provider' => 'table',
                    'message' => __('Email already exists.'),
                ]);
       
        $validator
            ->allowEmpty('password', 'create');
        
        /*$validator
            ->allowEmpty('confirm_password', 'create')
            ->add('confirm_password', 'custom', [
                'rule' => function($value, $context){              
                    if(!empty($value) && !empty($context['data']['password'])){                  
                        if ((new DefaultPasswordHasher)->check($value,$context['data']['password'])) {
                            return true;
                        }else{
                            return false;
                        }
                    }else{          
                        return false;
                    }
                },
                'message' => __('Password do not match')        
            ]);*/
            
        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    //public function buildRules(RulesChecker $rules)
    //{
    //    $rules->add($rules->isUniqueEmail(['email']));
    //    return $rules;
    //}
    
    public function isEmailExist($email, array $contect) {
        
        //In a controller or table method.
        $where = ['Users.email'=> $email, 'Users.is_deleted'=> 0];
        
        if(!empty($contect['data']['user_type_id'])){
            $where = array_merge($where,['Users.user_type_id'=>$contect['data']['user_type_id']]);
        }
        
        if(!empty($contect['data']['id'])){
            $where = array_merge($where,['Users.id !='=>$contect['data']['id']]);
        }        
        
        $emailExist = $this->find('all')->where($where)->select(['email'])->Count();
        return ($emailExist)? false:true;
        //pr($emailExist);die;
    }   
    
    public function isEmailExistForgotPassword($email) {
        //In a controller or table method.
       $emailExist = $this->find('all')->where(['Users.email'=> $email, 'Users.is_deleted'=> 0])->select(['email'])->Count();
       return ($emailExist)?'true':'false';
    }
    
    public function getUserByEmail($emailId = null){
        // In a controller or table method.
        $emailExist = [];
        if($emailId != NULL) {
            $emailExist = $this->find()
            ->where(['Users.email'=>$emailId,'Users.is_deleted'=>0])
            ->select(['Users.id','Users.email','Users.status','Users.is_deleted'])
            ->first();
        }
       
        return $emailExist;
    }    
    
    public function getUserById($Id = null){
       $userExist = $this->find()
        ->where(['Users.id'=>$Id,'Users.status'=> 1,'Users.is_block'=> 0,'Users.is_deleted'=> 0])
        ->select(['Users.id','Users.email','Users.full_name','Users.status','Users.is_deleted','Users.is_block','Users.stripe_customer_id','Users.stripe_account_id','Users.quickblox_id'])
        ->first();
        return $userExist;
    }    
   
    public function compareWith($password, array $contect) {        
        
        //In a controller or table method.
        $where = ['Users.email'=> $email, 'Users.is_deleted'=> 0,'Users.user_type_id'=>1];
        if(!empty($contect['data']['id'])){
            $where = array_merge($where,['Users.id !='=>$contect['data']['id']]);
        }        
        
       $emailExist = $this->find('all')->where($where)->select(['email'])->Count();
       return ($emailExist)? false:true;
       //pr($emailExist);die;
    }    
    
    function generate_password($length = 6) {
        $password = '';
        $chars = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));
        for ($i = 0; $i < $length; $i ++) {
            $password .= $chars[array_rand($chars)];
        }
        return $password;
    }
    
    /* 	
     * function name 	: getNearestDriver
     * Author        	: Inderjit Singh
     * Date          	: Sep 21, 2017     
     */

    function getNearestDriver($data) {

        $driver = $ORconditions = [];

        $latitude = $data['latitude'];
        $longitude = $data['longitude'];
        $miles = MILES_RADIUS;

        $distance_query = "(((acos(sin((" . $latitude . "*pi()/180)) * sin((Users.latitude*pi()/180))+cos((" . $latitude . "*pi()/180)) * cos((Users.latitude *pi()/180)) * cos(((" . $longitude . "- Users.longitude)*pi()/180))))*180/pi())*60*1.1515)";
        
        $in_progress_order = "(select count(id) from orders where orders.driver_id = Users.id and orders.is_deleted = 0 and orders.status in (1,2))";

        $conditions[] = "Users.user_type_id = 2";
        $conditions[] = "Users.online_status = 1";
        $conditions[] = "Users.is_login = 1";
        $conditions[] = "Users.status = 1";
        $conditions[] = "Users.is_deleted = 0";
        $conditions[] = "$distance_query < $miles";
        $conditions[] = "$distance_query >= 0";
        $conditions[] = "DriverFillings.tank_id = ".$data['tank_id'];
        $conditions[] = "DriverFillings.quantity >= ".$data['quantity'];
        $conditions[] = "$in_progress_order = 0";
        
        if (!empty($data['not_drivers'])) {
            $driverIds = implode("','", $data['not_drivers']);
            $conditions[] = "Users.id NOT IN ('$driverIds')";
        }
        if (!empty($data['include_driver'])) {
            $ORconditions['OR'][] = "Users.id = ".$data['include_driver'];
        }
        if(!empty($ORconditions)){
            $ORconditions['OR'][] = $conditions;
            $conditions = $ORconditions;
        }
        //prx($conditions);        
        
        $driver = $this->find()
                ->hydrate(false)
                ->where($conditions)
                ->select(['Users.id', 'Users.user_type_id', 'Users.first_name', 'Users.last_name', 'Users.email', 'Users.device', 'Users.device_token', 'distance' => $distance_query,'in_progress_order' => $in_progress_order])
                ->contain(['DriverFillings' => ['fields' => ['id', 'user_id', 'tank_id', 'quantity']]])
                ->toArray();        
        
        return $driver;
    }
    
    function convertToTimeZone($datetime, $from = 'UTC', $to = '', $output_format = 'Y-m-d H:i:s'){
        
        if(!empty($to)){
            
            $date = new \DateTime(date('Y-m-d H:i:s', strtotime($datetime)), new \DateTimeZone($from));            
			$date->setTimezone(new \DateTimeZone($to));						
            $datetime = $date->format('Y-m-d H:i:s');            
        }
        $formattedDate = date($output_format, strtotime($datetime));        
        return $formattedDate;
    }
}
